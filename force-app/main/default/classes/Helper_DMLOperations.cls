/********************************************************************************
 ********************************************************************************
 *  Class           : Helper_DMLOperations
 *  Author          : Eigen X
 *  Version History : 1.0
 *  Creation        : 07/27/2016
 *  Description     : Controller does the following :-
                      # Inserts a list of records
                      # Inserts a single record
                      # Updates a list of records
                      # Updates a single record
                      # Upserts a list of records
                      # Deletes a single  record
                      Deletes a list of  records
 ********************************************************************************
 ********************************************************************************/
public class Helper_DMLOperations {

	/**
	 * Insert Records
	 */
    public static List<String> insertRecords(List<SObject> records){
        Database.SaveResult[] lsr = Database.insert(records,false);
        List<String> errMessages = new List<String>();
        // Iterate through the Save Results
        for(Database.SaveResult sr:lsr){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                errMessages.add(err.getMessage());
            }
        }
        
        return errMessages;
    }

	 /**
	 * Insert a single Record
	 */
    public static String insertRecord(SObject record){
        Database.SaveResult sr = Database.insert(record,false);
        String errMessage = '';
        // Check the Save Result
        if(!sr.isSuccess()){
            Database.Error err = sr.getErrors()[0];
            errMessage = err.getMessage();
        }
        system.debug('Insert Records: Error Message = '+errMessage + 'Object Type = '+ record);
        return errMessage;
    }

	 /**
	 * Update Records
	 */
    public static List<String> updateRecords(List<SObject> records){
        Database.SaveResult[] lsr = Database.update(records,false);
        List<String> errMessages = new List<String>();
        // Iterate through the Save Results
        for(Database.SaveResult sr:lsr){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                errMessages.add(err.getMessage());
            }
        }
        return errMessages;
    }

	 /**
	 * Update a single Record
	 */
    public static String updateRecord(SObject record){
        Database.SaveResult sr = Database.update(record,false);
        String errMessage = '';
        // Check the Save Result
        if(!sr.isSuccess()){
            Database.Error err = sr.getErrors()[0];
            errMessage = err.getMessage();
        }
        system.debug('Update Record: Error Message = '+errMessage + 'Object Type = '+ record);
        return errMessage;
    }

	 /**
	 * Upsert Records
	 */
    public static List<String> upsertRecords(List<SObject> records){
        Database.UpsertResult[] lsr = Database.upsert(records,false);
        List<String> errMessages = new List<String>();
        // Iterate through the Save Results
        for(Database.UpsertResult sr:lsr){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                errMessages.add(err.getMessage());
            }
        }
        return errMessages;
    }

	  /**
	 * Deletes a single Record
	 */
    public static String deleteRecord(SObject record){
        Database.DeleteResult dr = Database.delete(record,false);
        String errMessage = '';
        // Check the Save Result
        if(!dr.isSuccess()){
            Database.Error err = dr.getErrors()[0];
            errMessage = err.getMessage();
        }
        return errMessage;
    }

	  /**
	 * Deletes a list of Records
	 */
    public static List<String> deleteRecords(List<SObject> records){
        Database.DeleteResult[] drList = Database.delete(records,false);
        List<String> errMessages = new List<String>();
        // Check the Save Result
        for(Database.DeleteResult delRslt:drList){
            if(!delRslt.isSuccess()){
                Database.Error err = delRslt.getErrors()[0];
                errMessages.add(err.getMessage());
            }
        }
        return errMessages;
    }
}