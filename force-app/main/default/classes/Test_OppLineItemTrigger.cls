@isTest
public with sharing class Test_OppLineItemTrigger {
	 public static OpportunityLineItem oppLineItem1;
    
     static testMethod void Test_OppLineItemTrigger_Main() 
     {
     	createTestData();
     	testDeleteOppLineItem();
        
        
    }
    
    static testMethod void createTestData()
    {
    	 //Build Dates 
            date mydate = date.parse('09/12/2017');
            date mydateMID = date.parse('10/12/2017');
            date mydateEND = date.parse('11/12/2019');
            
            //Build Account
            Account accountTest = new Account(
                Name='Aounty',
                Status__c='Prospect' ,
                Account_Type__c ='National'
            );    
            Helper_DMLOperations.insertRecord(accountTest);
            
            
            //Build DBA
            DBA__c dbaTest = new DBA__c (
                Name = 'DBA1', 
                Account__c = accountTest.Id
            );
            Helper_DMLOperations.insertRecord(dbaTest);
            
            
            //Get Standard Pricebook ID 
            Id pricebookMain = Test.getStandardPricebookId();
            
            
            //Build Opportunity
            Opportunity mainOpp = new Opportunity(
                Name='TestOp',
                AccountId= accountTest.id ,
                DBA__c = dbaTest.ID ,
                StageName= 'Negotiation/Review' ,
                Probability= 75 ,
                Type = 'New Account' ,
                Document_Type__c = 'Partnership Marketing' ,
                Deal_Type__c = 'Sponsorship' ,
                Partnership_Type__c = 'New Business' ,
                Possession_Date__c = mydateMID,
                Commencement_Date__c =mydateMID,
                Expiration_Date__c = mydateEND,
                CloseDate= mydate,
                Payment_Frequency__c = 'One-Time',
                Pricebook2Id = pricebookMain
            );
            Helper_DMLOperations.insertRecord(mainOpp);
            
            
            //Build Property 1
            Property__c propOne = new Property__c(
                Name = 'PROPONE', 
                Status__c = 'Active'
            );
            Helper_DMLOperations.insertRecord(propOne);
            
            
            //Build Property 2
            Property__c propTwo = new Property__c(
                Name = 'PROPTWO', 
                Status__c = 'Active'
            );
            Helper_DMLOperations.insertRecord(propTwo);
            
            
            /////////////////////////////Product/1/Items/////////////////////////////////////////////
            
            //Build Product2 #1
            Product2 oppProdOne = new Product2 (
                Name = 'ProdOne',
                Property__c = propOne.Id,
                ProductCode = 'B1',
                Bill_Code__c = 'CSP'
                //End_Date__c = date.parse('10/14/2017'),
                //Start_Date__c = date.parse('10/13/2017')
            );
            Helper_DMLOperations.insertRecord(oppProdOne);
            
            //Build Pricebook Entry #1
            PricebookEntry pricebookEntryOne = new PricebookEntry (
                Pricebook2Id = pricebookMain,
                Product2Id = oppProdOne.id,
                UnitPrice = 100, 
                isActive=TRUE
            ); 
            Helper_DMLOperations.insertRecord(pricebookEntryOne);
            
            
            //Build Opportunity line item
             oppLineItem1 = new OpportunityLineItem (
                OpportunityId = mainOpp.Id,
                PricebookEntryId = pricebookEntryOne.id,
                Quantity = 1.00,
                TotalPrice = 100,
                Start_Date__c = date.parse('10/13/2017'),
                End_Date__c = date.parse('10/14/2017')
            );
            Helper_DMLOperations.insertRecord(oppLineItem1);
            
           ///////////////////////////TESTING////////////////////////////////////////////////////// 
            
           
           //TEST 1 :start and end dates of opportunity line items within the same month
            
            // Get the line Items
           
    }
    
    static testMethod void testDeleteOppLineItem()
    {
    	try
    	{
    		Helper_DMLOperations.deleteRecord(oppLineItem1);
    		List <AllocationTableLineItem__c> lstLineItems = [SELECT MonthAmount__c FROM AllocationTableLineItem__c];
    		system.assertEquals(lstLineItems.size(),0 ,'DeleteOppLineItemsfailed' +lstLineItems.size() );
    		
    	}catch(Exception e)
    	{
    		System.debug('Exception in testDeleteOppLineItem'+e);
    	}
    	
    }
}