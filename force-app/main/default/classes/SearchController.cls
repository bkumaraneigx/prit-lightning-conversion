Public with sharing class SearchController{
 Public List<Opportunity> oppList{get;set;}
 Public List<contact>     conList{get;set;}
 Public List<dba__c>      dbaList{get;set;}
 Public List<account>     accList{get;set;}
 Public List<lead>        ledList{get;set;}
  
 Public String searchStr{get;set;}
   Public SearchController(){
   }
  
  Public void search_method(){
   oppList = New List<Opportunity>();
   conList = New List<contact>();
   dbaList = New List<dba__c>();
   accList = New List<account>();
   ledList = New List<lead>();
   
   if(searchStr.length() > 1){
   String searchStr1 = '%'+searchStr+'%';
   ledList = [select name, phone from lead where PREIT_Search__c like :searchstr1 order by name];
   accList = [select name, status__c from account where PREIT_Search__c LIKE :searchstr1 order by name];
   dbaList = [select name, status__c from dba__c where PREIT_Search__c LIKE :searchstr1 order by name];
   conList = [select name, phone from contact where PREIT_Search__c like :searchstr1 order by name];
   oppList = [select name, stagename from opportunity where PREIT_Search__c like :searchstr1 order by name];
 
   if(accList.size() == 0 && dbaList.size() == 0 && conList.size() == 0 && oppList.size() == 0){
       apexPages.addmessage(new apexpages.message(apexpages.severity.Error, 'Sorry, no results returned with matching string.'));
       return;
  }
   }
   else{
   apexPages.addmessage(new apexpages.message(apexpages.severity.Error, 'Please enter at least two characters.'));
   return;
   }
  }
}