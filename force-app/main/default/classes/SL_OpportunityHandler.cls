public with sharing class SL_OpportunityHandler {
    
    // global objects
    private Boolean isBeforeUpdate = false;
    
    // constructor
    public SL_OpportunityHandler(Boolean isExecuting, Integer size) { 
        // 2014-07-23 johnsondw I don't think these parameters are really needed
        //  as they are not referenced anywhere.
    }
   
    /*******************************************************************************
    * @MethodName   : confirmConsumption
    * @Scope        : private
    * @Returns      : void
    * @Arguments    : Set<Id> setProductIds, Id thisOppId
    * @Description  : make sure inventory is available; if not, issue error 
    ********************************************************************************/
    private void confirmConsumption(Set<Id> setProductIds, Id thisOppId) {
        // 2014-07-09 johnsondw Moved the comparison to the calculateExistingConsumption to test at line item level.
        // create a map of all products in this opportunity
        Map<Id, Product2> mapProducts = new Map<Id, Product2>([
                Select
                    Id
                ,   Availability__c
                ,   Name
                ,   Property__c
                from
                    Product2
                where
                    Id in: setProductIds
        ]);
        Property__c property;
        String inventoryName;
        String propName;
        Decimal availability = 0;
        Decimal consumed = 0;        
        // create a map of products that exceed available inventory
        Map<Id, Integer> mapProducts_Consumption = SL_Utility.calculateExistingConsumption(setProductIds, thisOppId);
        for (Id idProd : mapProducts_Consumption.keySet()) {
            inventoryName = mapProducts.get(idProd).Name;
            property = [Select Name From Property__c where Id =: mapProducts.get(idProd).Property__c]; 
            propName = property.Name;
            availability = mapProducts.get(idProd).Availability__c;
            consumed = mapProducts_Consumption.get(idProd);                 
            Trigger.new[0].addError(' Cannot be Closed; over commitment of Inventory: ' + inventoryName + ', ' + propName + ': ' + consumed + ' > '  + availability); 
        }
    }
    
    /*******************************************************************************
    * @MethodName   : onAfterInsert
    * @Scope        : public
    * @Returns      : void
    * @Arguments    : Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity
    * @Description  : Trigger event
    ********************************************************************************/
    public void onAfterInsert(Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity) {
        upsertPaymentSchedule(null, mapNewOpportunity);
    }

    /*******************************************************************************
    * @MethodName   : onAfterUpdate
    * @Scope        : public
    * @Returns      : void
    * @Arguments    : Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity
    * @Description  : Trigger event
    ********************************************************************************/
    public void onAfterUpdate(Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity) {
        validateAvailabilityOfInventory(mapOldOpportunity,mapNewOpportunity);
        upsertPaymentSchedule(mapOldOpportunity, mapNewOpportunity);
        updateOLI(mapOldOpportunity,mapNewOpportunity);
    }
    
    /*******************************************************************************
    * @MethodName   : onBeforeInsert
    * @Scope        : Public
    * @Returns      : void
    * @Arguments    : List<Opportunity> lstNewOpportunity
    * @Description  : Trigger event
    ********************************************************************************/
    public void onBeforeInsert(List<Opportunity> lstNewOpportunity) {
        isBeforeUpdate = true;
        updateFields(lstNewOpportunity);
    }

    /*******************************************************************************
    * @MethodName   : onBeforeUpdate
    * @Scope        : Public
    * @Returns      : void
    * @Arguments    : List<Opportunity> lstNewOpportunity
    * @Description  : Trigger event
    ********************************************************************************/
    public void onBeforeUpdate(List<Opportunity> lstNewOpportunity) {
        isBeforeUpdate = false;
        updateFields(lstNewOpportunity);
    }
    
    /*******************************************************************************
    * @MethodName   : updateFields
    * @Scope        : private
    * @Returns      : void
    * @Arguments    : List<Opportunity> lstNewOpportunity
    * @Description  : Initialize Opportunity properties
    ********************************************************************************/
    private void updateFields(List<Opportunity> lstNewOpportunity) {
        for(Opportunity obj : lstNewOpportunity) {
            if(obj.Payment_Frequency__c == '--None--' || obj.Payment_Frequency__c == '' || obj.Payment_Frequency__c == null)
                obj.Payment_Frequency__c = 'Quarterly';
            if(String.valueOf(obj.Amount) == '' || obj.Amount == null)
                obj.Amount = 0;
        }
    }
    
    /*******************************************************************************
    * @MethodName   : updateOLI
    * @Scope        : public
    * @Returns      : void
    * @Arguments    : Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity
    * @Description  : For certain stages, set the TaxID for the Account of the Opportunity in the OpportunityLineItems, 
    *                   and set the PushIntegration__c to true to initiate integration of the data to JDEdwards.
    ********************************************************************************/
    private void updateOLI(Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity) {
        // try
        // {
            map<Id, String> mapOppIDtoTax = new map<Id, String>();
            map<Id, Id> mapAccIDtoOppID = new map<Id, Id>();
            list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
            for(Opportunity o : mapNewOpportunity.values()) {
                if (o.StageName != mapOldOpportunity.get(o.Id).StageName
                    && (o.StageName == 'Closed Specialty Leasing' || o.StageName == 'Client Executed')) {
                    mapAccIDtoOppID.put(o.AccountID, o.ID);
                }
            }
            if (mapAccIDtoOppID.isempty()) {
                return;
            }
            // Do NOTHING FURTHER if the current user is the Integration User (automated process)
            List<Profile> thisProfile = [SELECT Id, Name FROM Profile WHERE Id =: userinfo.getProfileId() LIMIT 1];
            String ProfileName = thisProfile[0].Name;
            if (ProfileName == 'Custom - Integration User') {
                return;
            }
            for (Account a : [SELECT TaxID__c FROM Account WHERE Id IN :mapAccIDtoOppID.keyset()]) {
                mapOppIDtoTax.put(mapAccIDtoOppID.get(a.ID), a.TaxID__c); 
            }
            for (OpportunityLineItem oli : [
                    SELECT
                        Account_TaxID__c
                    ,   PushIntegration__c
                    ,   OpportunityID
                    FROM
                        OpportunityLineItem
                    WHERE
                        OpportunityID IN :mapOppIDtoTax.keyset()
                ]) {
                oli.PushIntegration__c = true;
                oli.Account_TaxID__c = mapOppIDtoTax.get(oli.OpportunityId); 
                lstOLI.add(oli);
            } 
            if (!lstOLI.isempty()) {
                update lstOLI;
            } 
        // }
        // catch(Exception e)
        // {
        //  system.debug(LoggingLevel.Error,'updateOLI: ' +e+' At line: '+ e.getLineNumber() );
        // }
    }
     
    /*******************************************************************************
    * @MethodName   : upsertPaymentSchedule
    * @Scope        : public
    * @Returns      : void
    * @Arguments    : Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity
    * @Description  : calculate the payment schedule for opportunity
    ********************************************************************************/
    public void upsertPaymentSchedule(Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity) {
        try {
            Map<Id, Integer> mapOpportunityId_NumberOfFrequency = new Map<Id, Integer>();                   //put number of months difference in map for specific opportunity 
            List<Payment_Schedule__c> lstPaymentSchedule = new List<Payment_Schedule__c>();                 //List of payment schedule to be inserted 
            List<Payment_Schedule__c> lstExistingPaymentSchedule = new List<Payment_Schedule__c>();
            Set<Id> setOpportunityIds = new Set<Id>();  
            ///Update case if Payment Schedule exists for the Opportunity, delete them and create new Payment schedules based on inputs
            if(mapOldOpportunity != null && mapOldOpportunity.size() > 0) {
                lstExistingPaymentSchedule = [Select Opportunity__c, Id From Payment_Schedule__c where Opportunity__r.Id IN: mapNewOpportunity.keySet()];
            }
            ///If Commencement date, expiration date, payment frequency, stage changes for an Opportunity, insert Payment schedule records 
            for(Opportunity objOpportunity : mapNewOpportunity.values()) {
                if(objOpportunity.Commencement_Date__c != null && String.valueOf(objOpportunity.Commencement_Date__c) != '' &&
                objOpportunity.Expiration_Date__c != null && String.valueOf(objOpportunity.Expiration_Date__c) != '' && objOpportunity.Expiration_Date__c < objOpportunity.Commencement_Date__c) {
                    mapNewOpportunity.get(objOpportunity.Id).addError('Expiration Date cannot be prior to Commencement Date. Please review.');
                }
                else if(lstExistingPaymentSchedule.isEmpty() && !isBeforeUpdate) {
                    if((objOpportunity.Commencement_Date__c != null && String.valueOf(objOpportunity.Commencement_Date__c) != '' &&
                    objOpportunity.Expiration_Date__c != null && String.valueOf(objOpportunity.Expiration_Date__c) != ''
                    && objOpportunity.Amount != null && String.valueOf(objOpportunity.Amount) != '' && (objOpportunity.StageName == 'Negotiation/Review' 
                        || objOpportunity.StageName == 'Contract Approved' || objOpportunity.StageName == 'Contract Sent' || objOpportunity.StageName == 'Client Executed'
                        || objOpportunity.StageName == 'Closed Won'))
                        && ((mapOldOpportunity != null && mapOldOpportunity.size() > 0  && objOpportunity.Total_for_Payment__c == mapOldOpportunity.get(objOpportunity.Id).Total_for_Payment__c) 
                        || (mapOldOpportunity == null || mapOldOpportunity.size() == 0))) {
                        ///If payment frequency is Monthly
                        if(objOpportunity.Payment_Frequency__c == 'Monthly') {
                            Integer actualTerm = objOpportunity.Commencement_Date__c.monthsBetween(objOpportunity.Expiration_Date__c);
                            if(objOpportunity.Expiration_Date__c.day() > objOpportunity.Commencement_Date__c.day()) 
                                actualTerm++;
                            mapOpportunityId_NumberOfFrequency.put(objOpportunity.Id, actualTerm);
                        }
                        ///If payment frequency is One time
                        else if(objOpportunity.Payment_Frequency__c == 'One-Time') {
                            mapOpportunityId_NumberOfFrequency.put(objOpportunity.Id, 1);
                        }
                        ///If payment frequency is Quarterly
                        else if(objOpportunity.Payment_Frequency__c == 'Quarterly') {
                            Integer diffMonths = objOpportunity.Commencement_Date__c.monthsBetween(objOpportunity.Expiration_Date__c);
                            if(objOpportunity.Expiration_Date__c.day() > objOpportunity.Commencement_Date__c.day()) 
                                diffMonths++;
                            Decimal actualTerm = diffMonths / 3;
                            mapOpportunityId_NumberOfFrequency.put(objOpportunity.Id, Integer.valueOf(actualTerm));
                        }
                        ///If payment frequency is Annually
                        else if(objOpportunity.Payment_Frequency__c == 'Annually') {
                            Integer diffMonths = objOpportunity.Commencement_Date__c.monthsBetween(objOpportunity.Expiration_Date__c);
                            if(objOpportunity.Expiration_Date__c.day() > objOpportunity.Commencement_Date__c.day()) 
                                diffMonths++;
                            Decimal actualTerm = diffMonths / 12; 
                            mapOpportunityId_NumberOfFrequency.put(objOpportunity.Id, Integer.valueOf(actualTerm));
                        }
                        Integer intMonth = objOpportunity.Commencement_Date__c.month();
                        Integer intYear = objOpportunity.Commencement_Date__c.year();
                        ///Create frequency for dates obtained in above steps
                        for(Integer i = 0; i < mapOpportunityId_NumberOfFrequency.get(objOpportunity.Id) ; i++) {
                            Payment_Schedule__c objPaymentSchedule = new Payment_Schedule__c();
                            objPaymentSchedule.Opportunity__c = objOpportunity.Id;
                            Decimal decAmount = (objOpportunity.Amount / mapOpportunityId_NumberOfFrequency.get(objOpportunity.Id));
                            decAmount = decAmount.setScale(2);
                            if(i == (mapOpportunityId_NumberOfFrequency.get(objOpportunity.Id) - 1)) {
                                Decimal decDivisions = (objOpportunity.Amount/ mapOpportunityId_NumberOfFrequency.get(objOpportunity.Id));
                                decDivisions = decDivisions.setScale(2);
                                Decimal decTotalAmountToBeReduced = (objOpportunity.Amount  - (decDivisions * mapOpportunityId_NumberOfFrequency.get(objOpportunity.Id)));
                                decTotalAmountToBeReduced = decTotalAmountToBeReduced.setScale(2);
                                if(decTotalAmountToBeReduced < 0)
                                    objPaymentSchedule.Amount__c = decAmount + decTotalAmountToBeReduced;
                                else 
                                     objPaymentSchedule.Amount__c = decAmount - decTotalAmountToBeReduced;
                            }
                            else
                                objPaymentSchedule.Amount__c = decAmount;
                            Date paymentDate = date.today();
                            if(i == 0)
                                paymentDate = Date.newinstance(intYear, intMonth, objOpportunity.Commencement_Date__c.day());
                            else
                                paymentDate = Date.newinstance(intYear, intMonth, 1);
                            objPaymentSchedule.Payment_Date__c = paymentDate;
                            if(objOpportunity.Payment_Frequency__c == 'Monthly')
                            intMonth++; 
                            //Increase month and year after december
                            if(intMonth > 12) {
                                intMonth = 1;
                                intYear++;
                            }
                            if(objOpportunity.Payment_Frequency__c == 'Quarterly') {
                                if(math.mod(intMonth, 3) == 2 || math.mod(intMonth, 3) == 1)
                                    intMonth = intMonth + 3;
                                else if(math.mod(intMonth, 3) == 0)
                                    intMonth  =  intMonth + 3;
                                if(intMonth >= 12) {
                                    intMonth = intMonth + 3;
                                    intMonth = math.mod(intMonth, 3);
                                    intYear++;
                                }
                            }
                            if(objOpportunity.Payment_Frequency__c == 'Annually')
                                intYear++;
                            lstPaymentSchedule.add(objPaymentSchedule);
                        }
                    }
                }
            }
            ///Insert list of payment Schedule
            if(!lstPaymentSchedule.isEmpty())
                insert lstPaymentSchedule;
        }
        Catch(Exception e) {
            system.debug('---------Exception--------'+e.getMessage()+'-------line number-------'+e.getLineNumber());
        }
    }
    
    /*******************************************************************************
    * @MethodName   : validateAvailabilityOfInventory
    * @Scope        : private
    * @Returns      : void
    * @Arguments    : Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity
    * @Description  : make sure inventory is available 
    ********************************************************************************/
    private void validateAvailabilityOfInventory(Map<Id, Opportunity> mapOldOpportunity, Map<Id, Opportunity> mapNewOpportunity) {
        set<String> setStageNames = new set<String>{'Client Executed','Closed Won'};
        Set<Id> setOppIds = new Set<Id>();
        Set<Id> setProductIds = new Set<Id>();
        Id thisOppId;        
        for (Opportunity objOpp : mapNewOpportunity.values()) {
            if(mapOldOpportunity.get(objOpp.Id).StageName != mapNewOpportunity.get(objOpp.Id).StageName
            && setStageNames.contains(mapNewOpportunity.get(objOpp.Id).StageName)) {   
                setOppIds.add(objOpp.Id);
                thisOppId = objOpp.Id;  
            }       
        }
        for (OpportunityLineItem objOLI : [
                select
                    PricebookEntry.Product2Id
                ,   OpportunityId
                ,   Opportunity.StageName
                ,   Quantity
                ,   Start_Date__c
                ,   End_Date__c 
                from
                    OpportunityLineItem
                where
                    OpportunityId In: setOppIds
            ]) {
            setProductIds.add(objOLI.PricebookEntry.Product2Id);
        }   
        confirmConsumption(setProductIds, thisOppId);
    }
    
   
   
}