/**
* \arg ClassName : SL_OpportunityInventoryRedirectExtension
* \arg JIRATicket : PREIT-3
* \arg CreatedOn : Sruti, 24/Aug/2013
* \arg ModifiedBy : Sruti, 24/Aug/2013
* \arg Description : Visualforce page to redirect to manage inventory if launched from edit Inventory
*/
public with sharing class SL_OpportunityInventoryRedirectExtension 
{
	
    Id oppId; 			//Opportunity Id sent to Manage Inventory Page
	Boolean isOLI;
	
    // We are extending the OpportunityLineItem controller, so we query to get the parent OpportunityId
    public SL_OpportunityInventoryRedirectExtension(ApexPages.StandardController controller) 
    {
        OpportunityLineItem objOLI = [select Id, OpportunityId,PricebookEntry.Product2Id from OpportunityLineItem where Id = :controller.getRecord().Id limit 1];
        oppId = objOLI.OpportunityId;
        isOLI = false;
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect()
    {
        return new PageReference('/apex/SL_ManageInventory?id=' + oppId+'&isOLI='+true);
    }

}