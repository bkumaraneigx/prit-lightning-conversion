public class AllProductsController {

    @AuraEnabled
    public static List<Product2> getAllProducts() {
        return [SELECT Availability__c,Description,Inventory_Type__c,Location__c,Name,Property__r.Name FROM Product2];
    }
    
    @AuraEnabled
    public static List<Product2> getFilteredProducts(String propertyStates, String propertyCities, String categories, String subCategories, Date startDate, Date endDate) {
        
        /*List<Product2> product2ByState = new List<Product2>();
        List<Product2> product2ByCity = new List<Product2>();
        List<Product2> product2ByCategory = new List<Product2>();
        List<Product2> product2BySubCategory = new List<Product2>();*/
        
        System.debug('propertystates are : ' + propertyStates);
        System.debug('propertyCities are : ' + propertyCities);
        System.debug('inventoryCategory are : ' + categories);
        System.debug('inventorySubCategories are : ' + subCategories);
        System.debug('startDate are : ' + startDate);
        System.debug('endDate are : ' + endDate);
        
        String[] propertyStatesArr = new String[]{};
        String[] propertyCitiesArr = new String[]{};
        String[] categoriesArr = new String[]{};
        String[] subCategoriesArr = new String[]{};
        if(propertyStates.length() > 0) {
                propertyStatesArr = propertyStates.split(',');
            }
        if(propertyCities.length() > 0) {
                propertyCitiesArr = propertyCities.split(',');
            }
        if(categories.length() > 0) {
                categoriesArr = categories.split(',');
            }
        if(subCategories.length() > 0) {
                subCategoriesArr = subCategories.split(',');
            }
        
        System.debug('propertystates Arr are : ' + propertyStatesArr);
        System.debug('propertyCities Arr are : ' + propertyCitiesArr);
        System.debug('inventoryCategory Arr are : ' + categoriesArr);
        System.debug('inventorySubCategories Arr are : ' + subCategoriesArr);
        //System.debug('startDate are : ' + startDate);
        //System.debug('endDate are : ' + endDate);
        
        
        String strQuery = '';
        strQuery = 'SELECT ' + 'id, qty__c, Description,Inventory_Type__c,Location__c,Name,Property__r.Name' ;
        strQuery += ', Availability__c, Start_date__c, End_Date__c, Property__r.Id, Rate_Type__c, Inventory_Categorization__c';
        strQuery += ' FROM Product2';
        strQuery += ' WHERE IsActive = true AND ( ( Start_Date__c != null AND End_Date__c != null AND Start_Date__c >= :startDate'; 
        strQuery += ' AND End_Date__c <= :endDate ) OR ( (End_Date__c = null) AND (Start_Date__c = null) ) )';
        if (propertyStatesArr.size() > 0) {
            strQuery += ' AND (Region__c IN :propertyStatesArr)';
        }
        if (propertyCitiesArr.size() > 0) {
            strQuery += ' AND (Property__c IN :propertyCitiesArr)';
        }
        if (categoriesArr.size() > 0) {
            strQuery += ' AND (Inventory_Categorization__c IN :categoriesArr)';
        }
        if (subCategoriesArr.size() > 0) {
            strQuery += ' AND (Name IN :subCategoriesArr)';
        }
        /*if (subCategoriesArr.size() > 0 && propertyStatesArr.size() == 0 )
            strQuery += ' AND (Name IN: subCategoriesArr) AND (Inventory_Categorization__c IN: categoriesArr)';
        else if ( subCategoriesArr.size() == 0 && categoriesArr.size() > 0 )
            strQuery += ' AND (Property__c IN: propertyCitiesArr)';
        else if (propertyCitiesArr.size() > 0 && subCategoriesArr.size() > 0)
            strQuery += ' AND (Property__c IN: propertyCitiesArr AND Name IN: subCategoriesArr)';*/
        strQuery += ' Limit 3';
        
        System.debug('**********************************************************');
        System.debug('Query Is : ' + strQuery);
        System.debug('**********************************************************');
        
        
        return Database.query(strQuery);  
    }
}