/*
 	Class-Name : ProductWrapperCls
 	Description : Used in SL_ManageInventoryController class

*/
///Wrapper class used to display product records along with checkbox and other fields 
public class ProductWrapperCls 
{
    public Long intQuantity             {get;set;}
    public Boolean isSelected           {get;set;}
    public SObject objSObject           {get;set;}
    public String intAmount             {get;set;}
    public Decimal intTotalAmount       {get;set;}
    public Integer intOLITotalQuantity  {get;set;}
    public String intUnitRate           {get;set;}
    public Date dtStartProduct          {get;set;}
    public Date dtEndProduct            {get;set;}
    public Id IdOLI                     {get;set;}  
    public Id objProductId              {get;set;}
    public Integer numberOfDays         {get;set;}
    public String strStartDate          {get;set;}
    public String strEndDate            {get;set;}
    
    public ProductWrapperCls(Long intQuantity, SObject objSObject, Boolean isSelected, String intAmount, Decimal intTotalAmount, Integer intOLITotalQuantity, String intUnitRate, Date dtStartProduct, Date dtEndProduct, Id IdOLI, Id objProductId, Integer numberOfDays, String strStartDate, String strEndDate)
    {
        this.intQuantity = intQuantity;
        this.objSObject = objSObject;
        this.isSelected = isSelected;
        this.intAmount = intAmount;
        this.intTotalAmount = intTotalAmount;
        this.intQuantity = intQuantity;
        this.intOLITotalQuantity = intOLITotalQuantity;
        this.intUnitRate = intUnitRate;
        this.dtStartProduct = dtStartProduct;
        this.dtEndProduct = dtEndProduct;
        this.IdOLI = IdOLI;
        this.objProductId = objProductId;
        this.numberOfDays = numberOfDays;
        this.strStartDate = strStartDate;
        this.strEndDate = strEndDate;
    }
}