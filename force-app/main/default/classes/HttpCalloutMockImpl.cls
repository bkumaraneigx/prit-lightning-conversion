@isTest
global class HttpCalloutMockImpl implements HttpCalloutMock 
{
    global HTTPResponse respond(HTTPRequest req) 
    {
    	String username = 'Test User';
    	String password = 'Test Pwd';
    	String jobName = 'Test JobName';
    	String jobType = 'Test JobType';
    	
        /*System.assertEquals('https://app.informaticaondemand.com/saas/api/1/runjob?username='+EncodingUtil.urlEncode(username, 'UTF-8')+
                           '&password='+EncodingUtil.urlEncode(password, 'UTF-8')+
                           '&jobName='+EncodingUtil.urlEncode(jobName, 'UTF-8')+
                           '&jobType='+EncodingUtil.urlEncode(jobType, 'UTF-8'), req.getEndpoint());*/
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        return res;
    }
}