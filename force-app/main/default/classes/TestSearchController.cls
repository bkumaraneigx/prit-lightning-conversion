@isTest(SeeAllData=true)
private with sharing class TestSearchController 
{
    static testMethod void myTest()
    {
        Test.startTest();
         // Set up the Contact record.
        Contact c = new Contact(FirstName='Tommy',LastName='Tester',MailingState='PA');
        insert c;
   
         // Set up the DBA record.
        DBA__c d = new DBA__c(Name='Test DBA');
        insert d;
   
         // Set up the Account record.
        Account a = new Account(Name='Test Account');
        insert a;
        
        // Set up the Lead record.
        Lead l = new Lead(FirstName='Test',LastName='Lead',Company='Test');
        insert l;
    
        // Set up the Opportunity record.
        String opportunityName = 'Test Opportunity';
        Opportunity o = new Opportunity(AccountId=a.Id, Name=opportunityName,
                                    StageName='Discovery', Deal_Type__c='Marketing NC',
                                    Document_type__c='Marketing NC', CloseDate=Date.today());
        insert o;

        Apexpages.currentPage().getParameters().put('name','phone');
        Apexpages.currentPage().getParameters().put('name','status');
        Apexpages.currentPage().getParameters().put('name','status');
        Apexpages.currentPage().getParameters().put('name','phone');
        Apexpages.currentPage().getParameters().put('name','stage');
        
        Pagereference testPage = Page.Search;
        Test.setCurrentPageReference(testPage);
        String SearchStr='test';
        
        SearchController ctrl = new SearchController();
        ctrl.searchStr = SearchStr;
        ctrl.search_method();

        Test.stopTest();
    }

}