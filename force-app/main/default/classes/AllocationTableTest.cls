/********************************************************************************
 ********************************************************************************
 *  Class           : Allocation Table Test
 *  Author          : Benjamin Potts (Eigen X)
 *  Version History : 1.0
 *  Creation        : 08/08/2016
 *  Description     : Creates Allocation Table records associated with an Opportunity between 60%
 *                      TEST 1 :start and end dates of opportunity line items within the same month
 * 						TEST 2: start and end dates in sequential months
 * 						TEST 3: start and end dates over a year in duration
 * 
 ********************************************************************************
 ********************************************************************************/


@isTest
public with sharing class AllocationTableTest {
   
    static testmethod void AllocationTableCreationSetup()
    {
        try
        {
            
            /////////////////////////////SETUP//ITEMS/////////////////////////////////////////////
            
            
            //Build Dates 
            date mydate = date.parse('09/12/2017');
            date mydateMID = date.parse('10/12/2017');
            date mydateEND = date.parse('11/12/2019');
            
            //Build Account
            Account accountTest = new Account(
                Name='Aounty',
                Status__c='Prospect' ,
                Account_Type__c ='National'
            );    
            Helper_DMLOperations.insertRecord(accountTest);
            
            
            //Build DBA
            DBA__c dbaTest = new DBA__c (
                Name = 'DBA1', 
                Account__c = accountTest.Id
            );
            Helper_DMLOperations.insertRecord(dbaTest);
            
            
            //Get Standard Pricebook ID 
            Id pricebookMain = Test.getStandardPricebookId();
            
            
            //Build Opportunity
            Opportunity mainOpp = new Opportunity(
                Name='TestOp',
                AccountId= accountTest.id ,
                DBA__c = dbaTest.ID ,
                StageName= 'Negotiation/Review' ,
                Probability= 75 ,
                Type = 'New Account' ,
                Document_Type__c = 'Partnership Marketing' ,
                Deal_Type__c = 'Sponsorship' ,
                Partnership_Type__c = 'New Business' ,
                Possession_Date__c = mydateMID,
                Commencement_Date__c =mydateMID,
                Expiration_Date__c = mydateEND,
                CloseDate= mydate,
                Payment_Frequency__c = 'One-Time',
                Pricebook2Id = pricebookMain
            );
            Helper_DMLOperations.insertRecord(mainOpp);
            
            
            //Build Property 1
            Property__c propOne = new Property__c(
                Name = 'PROPONE', 
                Status__c = 'Active'
            );
            Helper_DMLOperations.insertRecord(propOne);
            
            
            //Build Property 2
            Property__c propTwo = new Property__c(
                Name = 'PROPTWO', 
                Status__c = 'Active'
            );
            Helper_DMLOperations.insertRecord(propTwo);
            
            
            /////////////////////////////Product/1/Items/////////////////////////////////////////////
            
            //Build Product2 #1
            Product2 oppProdOne = new Product2 (
                Name = 'ProdOne',
                Property__c = propOne.Id,
                ProductCode = 'B1',
                Bill_Code__c = 'CSP'
                //End_Date__c = date.parse('10/14/2017'),
                //Start_Date__c = date.parse('10/13/2017')
            );
            Helper_DMLOperations.insertRecord(oppProdOne);
            
            //Build Pricebook Entry #1
            PricebookEntry pricebookEntryOne = new PricebookEntry (
                Pricebook2Id = pricebookMain,
                Product2Id = oppProdOne.id,
                UnitPrice = 100, 
                isActive=TRUE
            ); 
            Helper_DMLOperations.insertRecord(pricebookEntryOne);
            
            
            //Build Opportunity line item
            OpportunityLineItem oppLineItem1 = new OpportunityLineItem (
                OpportunityId = mainOpp.Id,
                PricebookEntryId = pricebookEntryOne.id,
                Quantity = 1.00,
                TotalPrice = 100,
                Start_Date__c = date.parse('10/13/2017'),
                End_Date__c = date.parse('10/14/2017')
            );
            Helper_DMLOperations.insertRecord(oppLineItem1);
            
           ///////////////////////////TESTING////////////////////////////////////////////////////// 
            
           
           //TEST 1 :start and end dates of opportunity line items within the same month
            
           List <AllocationTableLineItem__c> AlLineItems = [SELECT MonthAmount__c FROM AllocationTableLineItem__c]; // Get the line Items
           system.assertEquals(100, AlLineItems[0].MonthAmount__c ); //Assert that they are equal to predicted value
           
           //TEST 2: start and end dates in sequential months 
           
           oppLineItem1.End_Date__c = date.parse('11/14/2017'); //Change end date on oppLine Item
           Helper_DMLOperations.updateRecord(oppLineItem1); //Update OppLineItem
           Helper_DMLOperations.updateRecord(mainOpp); //Update Main Opp to trigger All Table Update
            
           List <AllocationTableLineItem__c> AlLineItems2 = [SELECT MonthAmount__c FROM AllocationTableLineItem__c]; //Query the Allocation Table Month Amounts  
           system.assertEquals(57.57575757575758, AlLineItems2[0].MonthAmount__c , 'Monthly amount should be 57.57575757575758. Instead its = '+ AlLineItems2[0].MonthAmount__c );  //Assert that the new value is accurate
           
  
           //TEST 3: start and end dates over a year in duration 
           
		   oppLineItem1.End_Date__c = date.parse('11/14/2018'); //Change end date on oppLine Item
           Helper_DMLOperations.updateRecord(oppLineItem1); //Update OppLineItem
           Helper_DMLOperations.updateRecord(mainOpp); //Update Main Opp to trigger All Table Update
            
           List <AllocationTableLineItem__c> AlLineItems3 = [SELECT MonthAmount__c FROM AllocationTableLineItem__c]; //Query the Allocation Table Month Amounts
           system.assertEquals(4.773869346733668, AlLineItems3[0].MonthAmount__c , 'Monthly amount should be 4.773869346733668 Instead its = '+ AlLineItems3[0].MonthAmount__c  );  //Assert that the new value is accurate           

            
        }
        catch(Exception e)
        {
            system.debug('Exception in Catch'+ e);
        }
        
        
        
    }
}