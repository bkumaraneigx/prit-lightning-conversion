@isTest
public with sharing class Test_ManageInventoryController_Part_2 {

    public static List<Opportunity> lstOpportunity;
    public static Property__c  property1;
    public static Product2 product1;
    public static Account account;
    public static DBA__c dba;
    public static Contact primaryContact;
    public static Contact signatory;
    public static List<Opportunity> listOpportunity;
    public static Opportunity opportunity1;
    public static PricebookEntry priceBookEntry1;
    public static OpportunityLineItem oppLineItem1_1;
    public static List<OpportunityLineItem> lstOLI;
    static testMethod void Test_ManageInventoryController_Part_2_Main() {

        createTestRecords();
        manageInventoryController_Test();
    }
   
    
    static testMethod void createTestRecords() {
        try {
            property1 = SL_UtilityCreateObjects.createProperty('Test Property 1'); 
            System.debug('insert property1');
            insert property1;

            product1 = SL_UtilityCreateObjects.createProduct('test Product 1', 50, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property1.Id, 'Signage', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product1');
            insert product1;
            
            account = SL_UtilityCreateObjects.createAccount('Test Account 1'); 
            account.TaxID__c = '584634231';
            account.Tax_Type__c = 'Corporate entity';
            System.debug('insert account');
            insert account;
            
            dba = SL_UtilityCreateObjects.createDBA(account.Id); 
            System.debug('insert dba');
            insert dba; 
            
            signatory= SL_UtilityCreateObjects.createContact('Test Signatory 1', account); 
            System.debug('insert signatory');
            insert signatory;
            
            lstOpportunity = new List<Opportunity>(); 
            
            opportunity1 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 1', date.today().addDays(-20), date.today().addDays(10), 'Annually', 'Discovery', account, dba.Id, signatory.Id);
            opportunity1.Payment_Frequency__c = '--None--';         
            opportunity1.Amount = 2.00;
            opportunity1.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity1');
            insert opportunity1;
            
            lstOpportunity.add(opportunity1);


            //Code added to get the pricebook id from the test class..
            Id spId = Test.getStandardPricebookId();
            System.debug('standard price book id: ' + spId);
            priceBookEntry1 = SL_UtilityCreateObjects.createPBE(spId, 15, product1.Id, false, true); 
            System.debug('insert priceBookEntry1');
            insert priceBookEntry1;
            
            
            lstOLI = new List<OpportunityLineItem>(); 


            oppLineItem1_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity1.Id,date.today().addDays(-20),date.today().addDays(10));
            oppLineItem1_1.PushIntegration__c = false;
            
            
            lstOLI.add(oppLineItem1_1);
            
            insert lstOLI;
            
            lstOpportunity[0].StageName = 'Closed Won';
            System.debug('update lstOpportunity1');
            update lstOpportunity;
            
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }
    }
    
    static testMethod void manageInventoryController_Test() {
        try {
            // Start of checking with functionality
            ApexPages.StandardController standardController1 = new ApexPages.standardController(lstOpportunity[0]);
            // Setting required parameters for page
            System.currentPageReference().getParameters().put('id', lstOpportunity[0].Id);
            SL_ManageInventoryController mic1 = new SL_ManageInventoryController(standardController1);
            
            // redirecting back to opportunity
            mic1.redirectLinkToOpportunity();
            
            ApexPages.StandardController standardController2 = new ApexPages.standardController(lstOpportunity[0]);
            System.currentPageReference().getParameters().put('id', lstOpportunity[0].Id);
            SL_ManageInventoryController mic2 = new SL_ManageInventoryController(standardController2);
            
           
           mic2.strSelectedValuesProperty = property1.Id;
           mic2.strSelectedValuesCategory = product1.Name;
           mic2.dtStartDate = date.today().addDays(-20);
           mic2.dtEndDate = date.today().addDays(10);
            
           system.assertEquals(mic2.lstWrapper.size(), 0,'lstWrapper.size() should = 0. lstwrapper.size = ' + mic2.lstWrapper.size());
           mic2.fetchProducts();
           system.debug('List Wrapper Size after fetch products: ' + mic2.lstWrapper.size());
           List<ProductWrapperCls> lstProdWrapper = new List<ProductWrapperCls>();
           Integer intProdCount = 0;
           for (ProductWrapperCls objWrapper :mic2.lstWrapper) 
           {
           		ProductWrapperCls setProdWrapper = objWrapper;
           		//Set the first product to quanity 1 and second to 0 to to simulate 2 products
           		if(intProdCount == 0)
           		{
           			setProdWrapper.intOLITotalQuantity = 0;
           			setProdWrapper.intQuantity= 0;
           			lstProdWrapper.add(setProdWrapper);
           		}else
           		{
           			setProdWrapper.intOLITotalQuantity = 1;
           			setProdWrapper.intQuantity= 1;
           			lstProdWrapper.add(setProdWrapper);	           			
           		}
           		//Increment the counter now.
           		intProdCount++;
           }
           //Change quantity and simulate screen value.
           mic2.lstWrapper = lstProdWrapper;

            mic2.AddSelectedRecords();
            mic2.RemoveSelectedRecords();
            PageReference pgRef = mic2.AddToOpportunity();
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}