/********************************************************************************
 ********************************************************************************
 *  Class           : Allocatino Table
 *  Author          : John Romano (Eigen X)
 *  Version History : 1.0
 *  Creation        : 08/02/2016
 *  Description     : Creates Allocation Table records associated with an Opportunity between 60%
 *                      & 90% when Inventory (opportunity Products) are present.
 *                      Creates child allocation table line item records based on
 *                      Inventory and the months of the contract
 *                      This replicates the billing schedule for all Opportunities that
 *                      have not closed but do have Inventory assocaited.
 *                      only 1 set of records are created and all if a change is made to the opportunity,
 *                      the allocation table is replaced with a new version.
 ********************************************************************************
 ********************************************************************************/


public with sharing class AllocationTable {

    //@InvocableVariable(label='OppID' required=true)
    public Id oppID;

    public class ATLineItems{
        OpportunityLineItem oppLineItemobj;
        Double monthlyAmount;
    }

    @InvocableMethod
    public static Void populateAllocationMap(List<Id> oppIds)
    {
        system.debug('INSIDE the ALLOCATION MAP');
        Map<String, List<ATLineItems>> allocationMaps = New Map<String, List<ATLineItems>>();
        List<ATLineItems> lstCurrentMapLineItem;
        Date startDateOfMonth;
        string pbOppID;
        try
        {

        for(ID oppID: oppIds)
        {
            List<OpportunityLineItem> lstOppLineItems =
            [SELECT End_Date__c,Id,OpportunityId,Product2Id,Property_Name__c,Name,Start_Date__c,TotalPrice,Property_SFDC_ID__c 
            FROM OpportunityLineItem WHERE OpportunityId = :oppID order by Start_Date__c asc];
            if (lstOppLineItems.size()>0)
            {
                pbOppID = oppID;
                for(OpportunityLineItem oppLItemObj: lstOppLineItems)
                {
                    Date stDate=oppLItemObj.Start_Date__c;
                    Date enDate=oppLItemObj.End_Date__c;
                    string oppLineItemID=oppLItemObj.ID;
                    set<string> monthyearKey=new set<string>();
                    Integer currentMonth;
                    Integer currentYear;
                    Double calculatedMonthlyAmount;
                    Double totalPriceLineItem = oppLItemObj.TotalPrice;
                    Date originalStartDate=oppLItemObj.Start_Date__c;
                    Date originalEndDate=oppLItemObj.End_Date__c;
					System.debug('product name'+oppLItemObj.name + 'stDate'+stDate +'endDate'+enDate);
					//Always get first date of the month and use it for comparison.
					startDateOfMonth = getStartDateofMonth(stDate);
                    //while(stDate<=enDate)
                    while(startDateOfMonth<=enDate)
                    {
                        string strMonthYearKey = stDate.month()+'-'+stDate.year();
                        currentMonth = stDate.month();
                        currentYear = stDate.year();
                        //Only Add to Month/Date Key if it is unique
                        if (!monthyearKey.contains(strMonthYearKey))
                        {
                            monthyearKey.add(stDate.month()+'-'+stDate.year());
                        }
                        calculatedMonthlyAmount = calculateMonthlyAmount(stDate,enDate,totalPriceLineItem,strMonthYearKey,currentMonth,currentYear, originalStartDate, originalEndDate);
                        
                        if (allocationMaps.containsKey(strMonthYearKey))
                            //for Start month get Start Month Year Key
                        {
                            ATLineItems atLineItemsobj = New ATLineItems();
                            atLineItemsobj.oppLineItemobj=oppLItemObj;
                            atLineItemsobj.monthlyAmount=calculatedMonthlyAmount;
                            lstCurrentMapLineItem = allocationMaps.get(strMonthYearKey);
                            lstCurrentMapLineItem.add(atLineItemsobj);
                            allocationMaps.put(strMonthYearKey,lstCurrentMapLineItem);
                            System.debug('allocation map contains' +strMonthYearKey + 'lstCurrentMapLineItem' +atLineItemsobj );
                        }
                        else
                                //For Each additional Month add new month year key
                        {
                            ATLineItems atLineItemsobj = New ATLineItems();
                            atLineItemsobj.oppLineItemobj=oppLItemObj;
                            atLineItemsobj.monthlyAmount=calculatedMonthlyAmount;
                            lstCurrentMapLineItem = new List<ATLineItems>();
                            lstCurrentMapLineItem.add(atLineItemsobj);
                            allocationMaps.put(strMonthYearKey,lstCurrentMapLineItem);
                            System.debug('allocation map else contains' +strMonthYearKey + 'lstCurrentMapLineItem' +atLineItemsobj );
                        }
                        
                        //Add one month to the start date so that you can get the allocation maps and loop through until start
                        //date of month is less than the end date.
                        stDate = stDate.addMonths(1);
                        startDateOfMonth = startDateOfMonth.addMonths(1);
                        
                        System.debug('after adding stdate product name'+oppLItemObj.name + 'stDate'+stDate +'endDate'+enDate +'startdateofmonth'+startDateOfMonth);
                    } //End of while loop stdate and enddate comparison


                }//Loop through each opp line item.
            }

        }
        
        CreateAllocationTable(allocationMaps,pbOppID);
        CreateAllocationTableLineItem(allocationMaps,pbOppID);
        } catch (exception e){
        }
    }
    public static Double calculateMonthlyAmount(Date stDate, Date enDate, Double totalPrice, string monthYearKey, Integer passCurrentMonth, Integer passCurrentYear, Date ogStart, Date ogEnd)
    {
        Integer totalDays=ogStart.daysBetween(ogEnd) + 1;
        if(totalDays == 0) {
            totalDays = 1;
        }
        Double dailyRate=totalPrice/totalDays;
        Integer intStMonth = stDate.month();
        Integer intStYear = stDate.year();
        String stDateMonthYear = stDate.month()+'-'+stDate.year();
        String enDateMonthYear = enDate.month()+'-'+enDate.year();
        String originMonthYearStart = ogStart.month()+'-'+ogStart.year();
        String originMonthYearEnd = ogEnd.month()+'-'+ogEnd.year();
        Integer totalDaysinCurrentMonth = 0;
        Double actualDays = 0;
        Double monthlyAmount = 0;


        if (monthYearKey.equals(originMonthYearStart))
            //Start Date Month
            {
                if (originMonthYearStart.equals(originMonthYearEnd)){
                    monthlyAmount = totalPrice;
                    system.debug('In If logic (Start month), Daily Rate -' + dailyRate + '; Monthly Amount - ' + monthlyAmount +'; Opportunity Line Item Month Year Start - ' + monthYearKey +'; Opportunity Line Item Month Year End - ' + originMonthYearEnd + ';');
                    return monthlyAmount;
                }
                else {
                    totalDaysinCurrentMonth = Date.daysInMonth(intStYear,intStMonth);
                    actualDays = totalDaysinCurrentMonth - stDate.day() + 1;
                    monthlyAmount = dailyRate * actualDays;
                    system.debug('In Else Logic (month not start or end month), Daily Rate -' + dailyRate + '; Monthly Amount - ' + monthlyAmount + '; Month = ' + intStMonth + '; Total Days in Current Month = ' + actualDays +';');
                    return monthlyAmount;
                }
                
            }
        else if (monthYearKey.equals(originMonthYearEnd))
            //end date month
            {
                actualDays = enDate.day();
                monthlyAmount = dailyRate * actualDays;
                system.debug('In Else If Logic (end Month), Daily Rate -' + dailyRate + '; Monthly Amount - ' + monthlyAmount + '; Month = ' + enDate.Month() + '; Total Days in Current Month = ' + actualDays +';');
                return monthlyAmount;
            }
        else //for months not start date or end date only
            {

                actualDays = Date.daysInMonth(passCurrentYear,passCurrentMonth);
                monthlyAmount = dailyRate * actualDays;
                system.debug('In Else Logic (for months not start date or end date only), Daily Rate -' + dailyRate + '; Monthly Amount - ' + monthlyAmount + '; Month = ' + PassCurrentMonth + '; Total Days in Current Month = ' + actualDays +';');
                return monthlyAmount;
            }


    }
    /*
            Method to retrieve to Create Allocation Table under the Opportunity prior to populating line items.
            – John 7/27/16

     */

    public static void CreateAllocationTable (Map<String, List<ATLineItems>> allocationMap, string oppID)
    {
        Set <string> monthYearKeys = allocationMap.keySet();
        List <AllocationTable__c> lstAllocationTables = new List <AllocationTable__c>();
        List <AllocationTable__c> lstExistingTables = new List <AllocationTable__c>();
        List <Opportunity> lstOpportunities = new List <Opportunity>();
        lstExistingTables=[SELECT Id FROM AllocationTable__c WHERE Opportunity__c =: oppID];
        lstOpportunities=[SELECT Name FROM Opportunity WHERE Id =: oppID];
        Opportunity retrievedOpportunity = lstOpportunities [0];
        if (lstExistingTables.size() > 0)
        {
            Helper_DMLOperations.deleteRecords(lstExistingTables);
        }

        for (string strMonthYearKey : monthYearKeys)
        {
            AllocationTable__c allocationTableMasterObj = new AllocationTable__c();
            allocationTableMasterObj.Name = 'Month - ' + strMonthYearKey;
            allocationTableMasterObj.Opportunity__c = oppID;
            allocationTableMasterObj.Month__c = strMonthYearKey.substringBefore('-');
            allocationTableMasterObj.Year__c = strMonthYearKey.substringAfter('-');
            allocationTableMasterObj.AllocationTableKey__c = strMonthYearKey;
            lstAllocationTables.add(allocationTableMasterObj);
        }
        Helper_DMLOperations.insertRecords(lstAllocationTables);
    }

    public static void CreateAllocationTableLineItem (Map<String, List<AtLineItems>> allocationMap, string oppID)
    {
        List <AllocationTable__c> lstAllocationTables = new List <AllocationTable__c>();
        lstAllocationTables = [SELECT Id ,Name, AllocationTableKey__c, Month__c, Year__c,Opportunity__c FROM AllocationTable__c WHERE Opportunity__c = :oppID];
        List <AllocationTableLineItem__c> lstChildAllocationTable = new List <AllocationTableLineItem__c>();
        List <ATLineItems> lstATLineItems;
        string strMasterKey;
        for (AllocationTable__c atObj:lstAllocationTables)
        {
            strMasterKey = atObj.AllocationTableKey__c;
            lstATLineItems = allocationMap.get(strMasterKey);
            string allocationTableID = atObj.Id;
            for (ATLineItems atLineItemObj:lstATLineItems)
            {
                AllocationTableLineItem__c allocationTableChildobj = new AllocationTableLineItem__c();
                allocationTableChildobj.AllocationTable__c = allocationTableID;
                allocationTableChildobj.Month__c = atObj.Month__c;
                allocationTableChildobj.Year__c = atObj.Year__c;
                allocationTableChildobj.Opportunity__c = atObj.Opportunity__c;
                allocationTableChildobj.Product__c = atLineItemObj.oppLineItemobj.Product2Id;
                allocationTableChildobj.MonthAmount__c = atLineItemObj.monthlyAmount;
                allocationTableChildobj.StartDate__c = atLineItemObj.oppLineItemobj.Start_Date__c;
                allocationTableChildobj.EndDate__c = atLineItemObj.oppLineItemobj.End_Date__c;
                allocationTableChildobj.Name = 'Month - ' + strMasterKey + ' - ' + atLineItemObj.oppLineItemobj.Property_Name__c;

                allocationTableChildobj.Property__c = atLineItemObj.oppLineItemobj.Property_SFDC_ID__c;
                //Need to create lookupfield on opportunity products for Property
                lstChildAllocationTable.add(allocationTableChildobj);

            }
        }
        Helper_DMLOperations.insertRecords(lstChildAllocationTable);
    }
    
    /*
    This method deletes all the allocation tables for an opp if an opportuity has zero opportunity line items
    */
    public static void deleteAllocationTablesForNoOppLI(List<string> lstOppId)
    {
    	List <AllocationTable__c> lstAllocationTables = [Select ID FROM AllocationTable__c WHERE  opportunity__c =: lstOppId];
    	Helper_DMLOperations.deleteRecords(lstAllocationTables);
    }
    
    /*
    */
    public static Date getStartDateofMonth(Date passedDate)
    {
    	Date startDateOfMonth = passedDate.toStartOfMonth();
    	return startDateOfMonth;
    }


}