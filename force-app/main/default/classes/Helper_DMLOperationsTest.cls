/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Helper_DMLOperationsTest {

    static testMethod void insertRecordTest() {
        Account accountTest = new Account(
                Name='Aounty',
                Status__c='Prospect' ,
                Account_Type__c ='National'
            );    
            
        
        Account accountTest2 = new Account(
                Name='Aounte',
                Status__c='Prospect' ,
                Account_Type__c ='National'
            );    
        
        Account accountTest3 = new Account(
                Name='',
                Status__c='Prospect' ,
                Account_Type__c ='National'
            );    
            
        list <Account> lstAccount = new list <Account> ();
        lstAccount.add(accountTest);
        lstAccount.add(accountTest2);
        Helper_DMLOperations.insertRecords(lstAccount);
        
        list <Account> lstAccount2 = [SELECT id FROM Account];
        Helper_DMLOperations.updateRecords(lstAccount2);
        
        Helper_DMLOperations.upsertRecords(lstAccount2);
        
        Helper_DMLOperations.deleteRecord(accountTest2);
        String errMessage = Helper_DMLOperations.insertRecord(accountTest3);
        System.assert(errMessage.length()>0, 'Error inserting records');
        accountTest2.Name = '';
        Helper_DMLOperations.updateRecord(accountTest2);
        String errMessage2 = Helper_DMLOperations.updateRecord(accountTest2);
        System.assert(errMessage2.length()>0, 'Error inserting records');
        
    }
}