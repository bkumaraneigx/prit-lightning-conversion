@isTest
public with sharing class Test_ManageInventoryController {

    public static List<Opportunity> lstOpportunity;
    public static Property__c  property1;
    public static Property__c  property2;
    public static Property__c  property3;
    public static Property__c  property4;
    public static Product2 product1;
    public static Product2 product2;
    public static Product2 product3;
    public static Product2 product4;
    public static Account account;
    public static DBA__c dba;
    public static Contact primaryContact;
    public static Contact signatory;
    public static List<Opportunity> listOpportunity;
    public static Opportunity opportunity1;
    public static Opportunity opportunity2;
    public static Opportunity opportunity3;
    public static Opportunity opportunity4;
    public static PricebookEntry priceBookEntry1;
    public static PricebookEntry priceBookEntry2;
    public static PricebookEntry priceBookEntry3;
    public static PricebookEntry priceBookEntry4;
    public static OpportunityLineItem oppLineItem1_1;
    public static OpportunityLineItem oppLineItem1_2;
    public static OpportunityLineItem oppLineItem1_3;
    public static OpportunityLineItem oppLineItem1_4;
    public static List<OpportunityLineItem> lstOLI;
         
    static testMethod void Test_ManageInventoryController_Main() {

        createTestRecords();
        manageInventoryController_Test();
        //testInventoryRedirectExt();
        
    }
    
    static void createTestRecords() {
        try {
            property1 = SL_UtilityCreateObjects.createProperty('Test Property 1'); 
            System.debug('insert property1');
            insert property1;
            /*
            property2 = SL_UtilityCreateObjects.createProperty('Test Property 2'); 
            System.debug('insert property2');
            insert property2;
            property2 = SL_UtilityCreateObjects.createProperty('Test Property 2'); 
            System.debug('insert property2');
            insert property2;
            property3 = SL_UtilityCreateObjects.createProperty('Test Property 3'); 
            System.debug('insert property3');
            insert property3;
            property4 = SL_UtilityCreateObjects.createProperty('Test Property 4');
            System.debug('insert property4');
            insert property4;
            */
            product1 = SL_UtilityCreateObjects.createProduct('test Product 1', 50, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property1.Id, 'Signage', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product1');
            insert product1;
            /*
            product2 = SL_UtilityCreateObjects.createProduct('test Product 2', 50, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property2.Id, 'Sponsorship', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product2');
            insert product2;
            product2 = SL_UtilityCreateObjects.createProduct('test Product 2', 50, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property2.Id, 'Sponsorship', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product2');
            insert product2;
            product3 = SL_UtilityCreateObjects.createProduct('test Product 3', 50, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property3.Id, 'Floor Space', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product3');
            insert product3;
            product4 = SL_UtilityCreateObjects.createProduct('test Product 4', 0, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property4.Id, 'Floor Space', 'New York', 'Food court tables', 'Monthly', true);
            System.debug('insert product4');
            insert product4;
			*/
            account = SL_UtilityCreateObjects.createAccount('Test Account 1'); 
            account.TaxID__c = '584634231';
            account.Tax_Type__c = 'Corporate entity';
            System.debug('insert account');
            insert account;
            
            dba = SL_UtilityCreateObjects.createDBA(account.Id); 
            System.debug('insert dba');
            insert dba; 
            
            signatory= SL_UtilityCreateObjects.createContact('Test Signatory 1', account); 
            System.debug('insert signatory');
            insert signatory;
            
            lstOpportunity = new List<Opportunity>(); 
            
            opportunity1 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 1', date.today().addDays(-20), date.today().addDays(10), 'Annually', 'Discovery', account, dba.Id, signatory.Id);
            opportunity1.Payment_Frequency__c = '--None--';         
            opportunity1.Amount = 2.00;
            opportunity1.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity1');
            insert opportunity1;
            //lstOpportunity.add(opportunity1);
            /*
            opportunity2 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 2', date.today().addDays(-20), date.today().addDays(10), 'Annually', 'Contract Sent', account, dba.Id, signatory.Id);
            opportunity2.Payment_Frequency__c = '--None--';         
            opportunity2.Amount = 2.00;
            opportunity2.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity2');
            insert opportunity2;
            lstOpportunity.add(opportunity1);
            lstOpportunity.add(opportunity2);
            
            
            opportunity2 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 2', date.today().addDays(-20), date.today().addDays(10), 'Annually', 'Contract Sent', account, dba.Id, signatory.Id);
            opportunity2.Payment_Frequency__c = '--None--';         
            opportunity2.Amount = 2.00;
            opportunity2.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity2');
            insert opportunity2;
            
            opportunity3 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 3', date.today().addDays(-20), date.today().addDays(10), 'Annually', 'Contract Approved', account, dba.Id, signatory.Id);
            opportunity3.Payment_Frequency__c = '--None--';         
            opportunity3.Amount = 2.00;
            opportunity3.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity3');
            insert opportunity3;

            opportunity4 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 4', date.today().addDays(-20), date.today().addDays(10), 'Annually', 'Contract Approved', account, dba.Id, signatory.Id);
            opportunity4.Payment_Frequency__c = '--None--';
            opportunity4.Amount = 2.00;
            opportunity4.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity4');
            insert opportunity4;
            
            lstOpportunity.add(opportunity1);
            lstOpportunity.add(opportunity2);
            lstOpportunity.add(opportunity3);
            lstOpportunity.add(opportunity4);
            // insert lstOpportunity;
            */
            /*
            List<PriceBook2> lstPriceBooks = [Select Id from PriceBook2 where name ='Standard Price Book' limit 2 for update];     
            Id spId;
            if (!lstPriceBooks.isEmpty()) { 
                spId = lstPriceBooks[0].Id;
            }
            */
            //Code added to get the pricebook id from the test class..
             Id spId = Test.getStandardPricebookId();
            //TBK 8/9
            System.debug('standard price book id: ' + spId);
            priceBookEntry1 = SL_UtilityCreateObjects.createPBE(spId, 15, product1.Id, false, true); 
            System.debug('insert priceBookEntry1');
            insert priceBookEntry1;
            /*
            priceBookEntry2 = SL_UtilityCreateObjects.createPBE(spId, 15, product2.Id, false, true); 
            System.debug('insert priceBookEntry2');
            insert priceBookEntry2;
            
            priceBookEntry2 = SL_UtilityCreateObjects.createPBE(spId, 15, product2.Id, false, true); 
            System.debug('insert priceBookEntry2');
            insert priceBookEntry2;
            priceBookEntry3 = SL_UtilityCreateObjects.createPBE(spId, 15, product3.Id, false, true); 
            System.debug('insert priceBookEntry3');
            insert priceBookEntry3;
            priceBookEntry4 = SL_UtilityCreateObjects.createPBE(spId, 15, product4.Id, false, true);
            System.debug('insert priceBookEntry4');
            insert priceBookEntry4;
            */
            lstOLI = new List<OpportunityLineItem>(); 

            //oppLineItem1_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity1.Id);
            oppLineItem1_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity1.Id,date.today().addDays(-20),date.today().addDays(10));
            oppLineItem1_1.PushIntegration__c = false;
            oppLineItem1_2 = SL_UtilityCreateObjects.createOLI(priceBookEntry2.Id, opportunity2.Id,date.today().addDays(-20),date.today().addDays(10)); 
            oppLineItem1_2.PushIntegration__c = false;
            lstOLI.add(oppLineItem1_1);
            system.debug('Before insert'+ lstOLI.size());
            insert lstOLI;
            /*
            oppLineItem1_2 = SL_UtilityCreateObjects.createOLI(priceBookEntry2.Id, opportunity2.Id); 
            oppLineItem1_2.PushIntegration__c = false;
            oppLineItem1_3 = SL_UtilityCreateObjects.createOLI(priceBookEntry3.Id, opportunity3.Id); 
            oppLineItem1_3.PushIntegration__c = false;
            oppLineItem1_4 = SL_UtilityCreateObjects.createOLI(priceBookEntry4.Id, opportunity4.Id);
            oppLineItem1_4.PushIntegration__c = false;*/
            
            /*
            lstOLI.add(oppLineItem1_2);
            
            lstOLI.add(oppLineItem1_2);
            lstOLI.add(oppLineItem1_3);
            lstOLI.add(oppLineItem1_4);
            System.debug('insert lstOLI');
            */
            
            
            lstOpportunity[0].StageName = 'Closed Won';
            System.debug('update lstOpportunity1');
            update lstOpportunity;
            /*
            lstOpportunity[1].StageName = 'Closed Won';
            System.debug('update lstOpportunity2');
            update lstOpportunity;
            lstOpportunity[2].StageName = 'Closed Won';
            System.debug('update lstOpportunity');
            update lstOpportunity;
            */
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }
    }
    
    static void manageInventoryController_Test() {
        try {
            // Start of checking with functionality
            ApexPages.StandardController standardController1 = new ApexPages.standardController(lstOpportunity[0]);
            // Setting required parameters for page
            System.currentPageReference().getParameters().put('id', lstOpportunity[0].Id);
            SL_ManageInventoryController mic1 = new SL_ManageInventoryController(standardController1);
            
            // redirecting back to opportunity
            mic1.redirectLinkToOpportunity();
            
            ApexPages.StandardController standardController2 = new ApexPages.standardController(lstOpportunity[0]);
            System.currentPageReference().getParameters().put('id', lstOpportunity[0].Id);
            SL_ManageInventoryController mic2 = new SL_ManageInventoryController(standardController2);
            
            //system.assertEquals(mic2.dtStartDate, lstOpportunity[0].Commencement_Date__c );
            //system.assertEquals(mic2.dtEndDate, lstOpportunity[0].Expiration_Date__c );
            // Checking with existing OLI's for Opportunity
            //system.assertEquals(mic2.lstWrapperSelected.size(), 1);
            
            ///Creatoing instance of product wrapper class
            
            
           // Test.startTest();
            
            
            // Searching products based on Property and Inventory selection.
           // mic2.strSelectedValuesProperty = property1.Id + ';' + property4.Id;
           // mic2.strSelectedValuesCategory = product1.Name + ';' + product4.Name;
           mic2.strSelectedValuesProperty = property1.Id + ';' + property2.Id;
           mic2.strSelectedValuesCategory = product1.Name + ';' + product2.Name ;
           mic2.dtStartDate = date.today().addDays(-20);
           mic2.dtEndDate = date.today().addDays(10);
            
           system.assertEquals(mic2.lstWrapper.size(), 0,'lstWrapper.size() should = 0. lstwrapper.size = ' + mic2.lstWrapper.size());
           mic2.fetchProducts();
           system.debug('List Wrapper Size after fetch products: ' + mic2.lstWrapper.size());
           List<ProductWrapperCls> lstProdWrapper = new List<ProductWrapperCls>();
           Integer intProdCount = 0;
           for (ProductWrapperCls objWrapper :mic2.lstWrapper) 
           {
           		ProductWrapperCls setProdWrapper = objWrapper;
           		//Set the first product to quanity 1 and second to 0 to to simulate 2 products
           		if(intProdCount == 0)
           		{
           			setProdWrapper.intOLITotalQuantity = 1;
           			setProdWrapper.intQuantity= 1;
           			lstProdWrapper.add(setProdWrapper);
           		}else
           		{
           			setProdWrapper.intOLITotalQuantity = 0;
           			setProdWrapper.intQuantity= 0;
           			lstProdWrapper.add(setProdWrapper);	
           			
           		}
           		//Increment the counter now.
           		intProdCount++;
           		
           		
           	
           }
           
           //Change quantity and simulate screen value.
           mic2.lstWrapper = lstProdWrapper;
           
            //system.assertEquals(mic2.lstWrapper.size(), 2,'lstWrapper.size() should = 2. lstwrapper.size = ' + mic2.lstWrapper.size());
            mic2.AddSelectedRecords();
            mic2.RemoveSelectedRecords();
            PageReference pgRef = mic2.AddToOpportunity();
            
            
            //Test.stopTest();
            
            //SL_Utility.calculateExistingConsumption_v2(mic2.lstWrapper, opportunity1.id);
            
            //mic2.refreshAvailability();
            
            // Checking with adding of products to selected section with out adding Any Quantity
            //system.assertEquals(mic2.lstWrapperSelected.size(), 1);
            /*mic2.AddSelectedRecords();
            mic2.RemoveSelectedRecords();
            PageReference pgRef = mic2.AddToOpportunity();*/
            /*

            // Searching products based on Inventory selection
            mic2.strSelectedValuesProperty = null;
            mic2.strSelectedValuesCategory = product1.Name + ';'  + product3.Name;
            mic2.strSelectedValuesCategoryName = product2.Inventory_Categorization__c;
            mic2.fetchProducts();
            //system.assertEquals(mic2.lstWrapper.size(), 1);
            
            mic2.strSelectedValuesProperty = null;
            mic2.strSelectedValuesCategory = null;
            mic2.strSelectedValuesCategoryName = null;
            mic2.fetchProducts();
            
            // Searching Products based on Property selection 
            mic2.strSelectedValuesProperty = property1.Id + ';' + property2.Id + ';' + property3.Id ;
            mic2.fetchProducts();
            //system.assertEquals(mic2.lstWrapper.size(), 3);
            
            // Checking with CurrentAvailability Of Searched products
            //system.assertEquals(mic2.lstWrapper[1].intOLITotalQuantity, 50);
            //system.assertEquals(mic2.lstWrapper[2].intOLITotalQuantity, 50);
            
            mic2.refreshAvailability();
            
            // Checking with adding of products to selected section with out adding Any Quantity
            //system.assertEquals(mic2.lstWrapperSelected.size(), 1);
            mic2.AddSelectedRecords();
            mic2.AddToOpportunity();
            mic2.RemoveSelectedRecords();
            //system.assertEquals(mic2.lstWrapperSelected.size(), 1);
            
            // Checking with adding of products to selected section by adding Quantity
            mic2.lstWrapper[0].intQuantity = 10;
            mic2.lstWrapper[1].intQuantity = 10;
            //system.assertEquals(mic2.lstWrapperSelected.size(), 1);
            mic2.AddSelectedRecords();
            //system.assertEquals(mic2.lstWrapperSelected.size(), 3);
            //system.assertEquals(mic2.lstWrapper.size(),3);
            
            // checking with removing of records from Selected section
            mic2.lstWrapperSelected[2].isSelected = true;
            mic2.RemoveSelectedRecords();

            Test.stopTest();
            //system.assertEquals(mic2.lstWrapperSelected.size(), 2);
            //system.assertEquals(mic2.lstWrapper.size(),3);

            /*list<OpportunityLineItem> lstOLIs = [
                Select
                    OpportunityId
                from
                    OpportunityLineItem 
                where
                    OpportunityId =: lstOpportunity[0].id
            ];*/
            //system.assert(lstOLIs.size() == 1);*/
            
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }
    }
    
     /*
    //This method will test the inventory redirect extension
    static void testInventoryRedirectExt()
    {
        	system.debug('In test inventory = '+ lstOLI.size());
    	 	ApexPages.StandardController standardController3 = new ApexPages.standardController(lstOLI[0]);//getting a null pointer exception here 
            System.currentPageReference().getParameters().put('id', lstOLI[0].Id);
            SL_OpportunityInventoryRedirectExtension  mic3 = new SL_OpportunityInventoryRedirectExtension (standardController3);
            PageReference pgRef = mic3.redirect();
    	
    }*/
    
}