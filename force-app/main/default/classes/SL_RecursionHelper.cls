/**
* @ClassName    : SL_RecursionHelper 
* @JIRATicket   : FAEF-14
* @CreatedOn    : Sruti, 12/Aug/2013
* @ModifiedBy   : Sruti, 16/Aug/2013
* @Description  : This is recursion class used in equipment trigger to avoid recursion in before and after triggers
*/

/**
@Developer Name							: Sruti
Percentage of best practices followed	: 100%
No of SOQL queries used					: 0
No of collections used					: 0
Exception Handling implemented			: No
Coding standards followed				: Yes
Naming conventions followed				: Yes
Third party integrations				: No
Maximum of No of records tested with	: 
Customer Approval						: 
Last Modified Date						: 
Approved by								: 

*/

public with sharing class SL_RecursionHelper 
{
	///This boolean will be set to true by default
	public static boolean isRecursiveInsert = false;
	
	/*
		@MethodName : getisRecursive
		@param      : NA
		@Description: This method is used to get the value of the isRecursive variable from the calling method.
	*/
	public static boolean hasAlreadyCreatedOpportunity() {
		return isRecursiveInsert;
	}
	
	public static void setAlreadyCreatedOpportunity() {
		isRecursiveInsert = true;
	}
	
	///This boolean will be set to true by default
	public static boolean isRecursive = false;
	
	/*
		@MethodName : getisRecursive
		@param      : NA
		@Description: This method is used to get the value of the isRecursive variable from the calling method.
	*/
	public static boolean hasAlreadyUpdatedOpportunity() {
		return isRecursive;
	}
	
	public static void setAlreadyUpdatedOpportunity() {
		isRecursive = true;
	}
}