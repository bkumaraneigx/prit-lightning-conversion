public with sharing class PropertyComponentController {
    
    public class RegionLocations {
        public String regionName;
        public String propertyName;
    }
  
  @AuraEnabled
  public static List<String> getRegions() {
    String ObjectApi_name = 'Product2';
	String Field_name = 'Region__c';
	List<String> lstPickvals=new List<String>();
  	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
    Sobject Object_name = targetType.newSObject();
  	Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
    List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
    for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
      lstPickvals.add(a.getValue());//add the value  to our final list
   	}
	return lstPickvals;
      
      
      /* Map<String,String> Regions = new Map<String,String>();
      List<Product2> p2s = [SELECT Region__c, Id FROM Product2 where Region__c != Null AND IsActive = true ORDER BY Region__c];
      for (Integer i=0; i < p2s.size(); i++) {
          string rname = p2s[i].Region__c;
          Regions.put(String.valueOf(p2s[i].Id), rname);
          while(i<p2s.size() && p2s[i].Region__c == rname) {
              //if (!(i > (p2s.size()))) {
                  i++;
              //}
          }
          i--;
      }
      return Regions;
      //return p2s; */
  }
    
  @AuraEnabled
   public static List<Product2> getRegionLocations() {
    List<Product2> p2s = [SELECT Property__c, Property__r.Name, Region__c, Name,Id FROM Product2 where Region__c != Null AND IsActive = true AND Property__c != null ORDER BY Region__c];
      /* for (Product2 p2 : p2s) {
           p2.property_name__c = 'Spring Field';
       } */     
     return p2s;  
  }
}