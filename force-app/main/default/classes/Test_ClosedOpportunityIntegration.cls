@isTest(SeeAllData=true)
public with sharing class Test_ClosedOpportunityIntegration {
         
    static testMethod void Test_ClosedOpportunityIntegration_TriggerCallOut() {
        testCallout();
    }
    
    // testing for callout of Opportunity trigger - PREIT-6
    public static void testCallout() {
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMockImpl());
        String username = 'Test User';
        String password = 'Test Pwd';
        String jobName = 'Test JobName';
        String jobType = 'Test JobType';
        ClosedOpportunityIntegration.runjob(username, password, jobName, jobType);
    }
    
}