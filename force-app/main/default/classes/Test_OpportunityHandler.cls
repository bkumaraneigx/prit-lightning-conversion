@isTest
public with sharing class Test_OpportunityHandler {

    public static Property__c  objProperty1;
    public static Property__c  objProperty2;
    public static Property__c  objProperty3;
    public static Product2 objProduct1;
    public static Product2 objProduct2;
    public static Product2 objProduct3;
    public static Account objAccount;
    public static DBA__c objDBA;
    public static Contact objContact;
    public static List<Opportunity> lstOpportunity;
    public static Property__c  property1;
    public static Property__c  property2;
    public static Property__c  property3;
    public static Product2 product1;
    public static Product2 product2;
    public static Product2 product3;
    public static Account account;
    public static DBA__c dba;
    public static Contact primaryContact;
    public static Contact signatory;
    public static List<Opportunity> listOpportunity;
    public static Opportunity opportunity1;
    public static Opportunity opportunity2;
    public static Opportunity opportunity3;
    public static PricebookEntry priceBookEntry1;
    public static PricebookEntry priceBookEntry2;
    public static PricebookEntry priceBookEntry3;
    public static OpportunityLineItem oppLineItem1_1;
    public static OpportunityLineItem oppLineItem1_2;
    public static OpportunityLineItem oppLineItem1_3;
    public static OpportunityLineItem oppLineItem2_1;
    public static OpportunityLineItem oppLineItem2_2;
    public static OpportunityLineItem oppLineItem2_3;
         
    static testMethod void Test_OpportunityHandler_Main() {
        createProperty_Test();
        createProduct_Test();
        createAccount_DBA_Test();
        createContact_Test();
        createOpportunity_Test();
        Test.startTest();
        createPBE_Test();
        createOLI_Test();
        testError_TaxIdNumeric();
        testError_taxIdValid();
        testError_taxTypeRequired();
        testError_accountClean();
        Test.stopTest();
    }
    
  public static void createProperty_Test() {
        String methodName = 'createProperty_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'some type of problem';
        try {
            property1 = SL_UtilityCreateObjects.createProperty('Test Property 1'); 
            System.debug('insert property1');
            insert property1;
            /*property2 = SL_UtilityCreateObjects.createProperty('Test Property 2'); 
            System.debug('insert property2');
            insert property2;
            property3 = SL_UtilityCreateObjects.createProperty('Test Property 3'); 
            System.debug('insert property3');
            insert property3;*/
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
    public static void createProduct_Test() {
        String methodName = 'createProduct_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'some type of problem';
        try {
            // public static Product2 createProduct(String productName, Integer intAvailability, String strBillCode, Date startDate, Date endDate, String strInventoryType, Id inventoryId, String strCategory, String strRegion, String strLocation, String strRateType, Boolean blnActive)
            product1 = SL_UtilityCreateObjects.createProduct('test Product 1', 10, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property1.Id, 'Signage', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product1');
            insert product1;
            /*product2 = SL_UtilityCreateObjects.createProduct('test Product 2', 20, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property2.Id, 'Sponsorship', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product2');
            insert product2;
            product3 = SL_UtilityCreateObjects.createProduct('test Product 3', 30, 'BC12', date.today().addDays(-20), date.today().addDays(10), 'Unit', property3.Id, 'Floor Space', 'New York', 'Food court tables', 'Monthly', true); 
            System.debug('insert product3');
            insert product3;*/
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
   public static void createAccount_DBA_Test() {
        String methodName = 'createAccount_DBA_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'some type of problem';
        try {
            account = SL_UtilityCreateObjects.createAccount('Test Account 1'); 
            System.debug('insert account');
            insert account;

            dba = SL_UtilityCreateObjects.createDBA(account.Id); 
            System.debug('insert dba');
            insert dba; 
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
        
   public static void createContact_Test() {
        String methodName = 'createContact_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'Post Office two-letter state abbreviation';
        String knownError03 = 'some type of problem';
        try {
            // create primaryContact
            primaryContact = SL_UtilityCreateObjects.createContact('Test Contact 1', account); 
            System.debug('insert primaryContact');
            insert primaryContact;
            // create Signatory
            signatory = SL_UtilityCreateObjects.createContact('Test Signatory 1', account); 
            System.debug('insert signatory');
            insert signatory;
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
        
    public static void createOpportunity_Test() {
        String methodName = 'createOpportunity_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'some type of problem';
        try {
            // create Opportunity from above objects
            opportunity1 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 1', date.today().addDays(10), date.today().addDays(100), 'Annually', 'Discovery', account, dba.Id, signatory.Id);
            // opportunity1.Payment_Frequency__c = '--None--';        
            // opportunity1.Amount = null;
            // opportunity1.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity1');
            insert opportunity1;
            /*opportunity2 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 2', date.today().addDays(10), date.today().addDays(100), 'Annually', 'Discovery', account, dba.Id, signatory.Id);
            // opportunity2.Payment_Frequency__c = '--None--';        
            // opportunity2.Amount = null;
            // opportunity2.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity2');
            insert opportunity2;
            opportunity3 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 3', date.today().addDays(10), date.today().addDays(100), 'Annually', 'Discovery', account, dba.Id, signatory.Id);
            // opportunity3.Payment_Frequency__c = '--None--';        
            // opportunity3.Amount = null;
            // opportunity3.Document_Type__c = 'Partnership Marketing';
            System.debug('insert opportunity3');
            insert opportunity3;*/
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
    
   
    
   public static void createPBE_Test() {
        String methodName = 'createPBE_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'some type of problem';
        //Code added to get the pricebook id from the test class..
        Id spId = Test.getStandardPricebookId();
        //TBK 8/9
        try {
            priceBookEntry1 = SL_UtilityCreateObjects.createPBE(spId, 100, product1.Id, false, true);
            System.debug('insert priceBookEntry1');
            insert priceBookEntry1;
            /*priceBookEntry2 = SL_UtilityCreateObjects.createPBE(spId, 200, product2.Id, false, true);
            System.debug('insert priceBookEntry2');
            insert priceBookEntry2;
            priceBookEntry3 = SL_UtilityCreateObjects.createPBE(spId, 300, product3.Id, false, true);
            System.debug('insert priceBookEntry3');
            insert priceBookEntry3;*/
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                //System.assertEquals(expectedExceptionFound, true);
                System.assertEquals(expectedExceptionFound, false);

            }
        }
    }
    
   public static void createOLI_Test() {
        String methodName = 'createOLI_Test';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'some type of problem';
        try {
            oppLineItem1_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity1.Id);
            oppLineItem1_1.PushIntegration__c = false;
            System.debug('insert oppLineItem1_1');
            insert oppLineItem1_1;
            /*oppLineItem1_2 = SL_UtilityCreateObjects.createOLI(priceBookEntry2.Id, opportunity1.Id);
            oppLineItem1_2.PushIntegration__c = false;
            System.debug('insert oppLineItem1_2');
            insert oppLineItem1_2;
            oppLineItem1_3 = SL_UtilityCreateObjects.createOLI(priceBookEntry3.Id, opportunity1.Id);
            oppLineItem1_3.PushIntegration__c = false;
            System.debug('insert oppLineItem1_3');
            insert oppLineItem1_3;

            oppLineItem2_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity2.Id);
            oppLineItem2_1.PushIntegration__c = false;
            System.debug('insert oppLineItem2_1');
            insert oppLineItem2_1;
            oppLineItem2_2 = SL_UtilityCreateObjects.createOLI(priceBookEntry2.Id, opportunity2.Id);
            oppLineItem2_2.PushIntegration__c = false;
            System.debug('insert oppLineItem2_2');
            insert oppLineItem2_2;
            oppLineItem2_3 = SL_UtilityCreateObjects.createOLI(priceBookEntry3.Id, opportunity2.Id);
            oppLineItem2_3.PushIntegration__c = false;
            System.debug('insert oppLineItem2_3');
            insert oppLineItem2_3;*/
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
                //System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
                //System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                //System.assertEquals(expectedExceptionFound, true);
                System.assertEquals(expectedExceptionFound, false);//Old code asserted value = True... for unknown reasons
            }
        }
    }
    
    static void testError_TaxIdNumeric() {
        String methodName = 'testError_TaxIdNumeric';
        String knownError01 = 'Tax ID on Account must be numeric';
        String knownError02 = 'Tax type is required if a tax id has been entered';
        String knownError03 = 'Please enter a VALID Tax number for the Account';
        try {
            update account;
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (ex.getMessage().contains(knownError03)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError03);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, false);
                //System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
    static void testError_taxIdValid() {
        String methodName = 'testError_taxIdValid';
        String knownError01 = 'Tax ID on Account must be numeric';
        String knownError02 = 'Tax type is required if a tax id has been entered';
        String knownError03 = 'Please enter a VALID Tax number for the Account';
        try {
            account.TaxID__c = '132436';
            System.debug('update account to fix taxId not numeric error');
            update account;
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (ex.getMessage().contains(knownError03)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError03);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, false);
                //System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
    static void testError_taxTypeRequired() {
        String methodName = 'testError_taxTypeRequired';
        String knownError01 = 'Tax ID on Account must be numeric';
        String knownError02 = 'Tax type is required if a tax id has been entered';
        String knownError03 = 'Please enter a VALID Tax number for the Account';
        try {
            account.TaxID__c = '132436485';
            System.debug('update account to fix taxId not valid');
            update account;
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (ex.getMessage().contains(knownError03)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError03);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, false);
                //System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
    static void testError_accountClean() {
        String methodName = 'testError_accountClean';
        String knownError01 = 'Tax type is required if a tax id has been entered';
        try {
            account.Tax_Type__c = 'Corporate entity';
            System.debug('update account to fix Tax type required error');
            update account;
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
            System.assertEquals(expectedExceptionFound, true);
        }
    }
    
    static void testError_overcommitmentOfInventory() {
        String methodName = 'testError_overcommitmentOfInventory';
        String knownError01 = 'over commitment of Inventory';
        String knownError02 = 'Tax ID on Account must be numeric';
        try {
            // create OpportunityLineItems
            oppLineItem1_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity1.Id);
            oppLineItem1_1.Start_Date__c = date.today().addDays(10);
            oppLineItem1_1.End_Date__c = date.today().addDays(100);
            oppLineItem1_1.Quantity = 10;
            oppLineItem1_1.PushIntegration__c = false;
            System.assertEquals(oppLineItem1_1.Quantity, product1.Availability__c);
            System.debug('insert oppLineItem1_1');
            insert oppLineItem1_1;
            
            /*oppLineItem1_2 = SL_UtilityCreateObjects.createOLI(priceBookEntry2.Id, opportunity1.Id);
            oppLineItem1_2.Start_Date__c = date.today().addDays(10);
            oppLineItem1_2.End_Date__c = date.today().addDays(100);
            oppLineItem1_2.Quantity = 10;
            oppLineItem1_2.PushIntegration__c = false;
            System.debug('insert oppLineItem1_2');
            insert oppLineItem1_2;
            
            oppLineItem1_3 = SL_UtilityCreateObjects.createOLI(priceBookEntry3.Id, opportunity1.Id);
            oppLineItem1_3.Start_Date__c = date.today().addDays(10);
            oppLineItem1_3.End_Date__c = date.today().addDays(100);
            oppLineItem1_3.Quantity = 10;
            oppLineItem1_3.PushIntegration__c = false;
            System.debug('insert oppLineItem1_3');
            insert oppLineItem1_3;*/
        
            opportunity1.StageName = 'Client Executed';
            opportunity1.Payment_Frequency__c = 'Monthly';
            opportunity1.Amount = 1000;
            System.debug('update opportunity1');
            update opportunity1;
        
            /*opportunity2 = SL_UtilityCreateObjects.createOpportunity('Test Opportunity 2', date.today().addDays(10), date.today().addDays(100), 'Annually', 'Contract Sent', account, dba.Id, signatory.Id);
            opportunity2.StageName = 'Contract Sent';
            System.debug('insert opportunity2');
            insert opportunity2;
        
            // create OpportunityLineItems
            oppLineItem2_1 = SL_UtilityCreateObjects.createOLI(priceBookEntry1.Id, opportunity2.Id);
            oppLineItem2_1.Start_Date__c = date.today().addDays(10);
            oppLineItem2_1.End_Date__c = date.today().addDays(100);
            oppLineItem2_1.Quantity = 10;
            oppLineItem2_1.PushIntegration__c = false;
            System.debug('insert oppLineItem2_1');
            insert oppLineItem2_1;
            
            oppLineItem2_2 = SL_UtilityCreateObjects.createOLI(priceBookEntry2.Id, opportunity2.Id);
            oppLineItem2_2.Start_Date__c = date.today().addDays(10);
            oppLineItem2_2.End_Date__c = date.today().addDays(100);
            oppLineItem2_2.Quantity = 10;
            oppLineItem2_2.PushIntegration__c = false;
            System.debug('insert oppLineItem2_2');
            insert oppLineItem2_2;
            
            oppLineItem2_3 = SL_UtilityCreateObjects.createOLI(priceBookEntry3.Id, opportunity2.Id);
            oppLineItem2_3.Start_Date__c = date.today().addDays(10);
            oppLineItem2_3.End_Date__c = date.today().addDays(100);
            oppLineItem2_3.Quantity = 10;
            oppLineItem2_3.PushIntegration__c = false;
            System.debug('insert oppLineItem2_3');
            insert oppLineItem2_3;

            System.debug('OLI2_1.Start: ' + oppLineItem2_1.Start_Date__c);
            System.debug('OLI1_1.Start: ' + oppLineItem1_1.Start_Date__c);
            System.debug('OLI2_1.End: ' + oppLineItem2_1.End_Date__c);
            System.debug('OLI1_1.End: ' + oppLineItem1_1.End_Date__c);
        
            opportunity2.StageName = 'Client Executed';
            opportunity2.Payment_Frequency__c = 'Monthly';
            opportunity2.Amount = 1000;
            System.debug('update opportunity2');
            update opportunity2;*/
        }
        catch(Exception ex) {
            Boolean expectedExceptionFound = false;
            if (ex.getMessage().contains(knownError01)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError01);
                System.assertEquals(expectedExceptionFound, true);
            } 
            if (ex.getMessage().contains(knownError02)) {
                expectedExceptionFound = true;
                System.debug(methodName + ': Expected exception Found: ' + knownError02);
                System.assertEquals(expectedExceptionFound, true);
            }
            if (!expectedExceptionFound) {
                System.debug(methodName + ': unexpected exception: ' + ex.getMessage());
                System.assertEquals(expectedExceptionFound, false);
                //System.assertEquals(expectedExceptionFound, true);
            }
        }
    }
    
}