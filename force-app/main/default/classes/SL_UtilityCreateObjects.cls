public without sharing class SL_UtilityCreateObjects 
{
    /*start : Create Objects required for test classes*/
    
    ///Creating Opportunity
    public static Opportunity createOpportunity(String Name, Date dtStartDate, Date dtEndDate, String PaymentFrequency, String StageName, Account objAccount, Id dbaId, Id contactId)
    {
        Opportunity objOpportunity = new Opportunity(Name = Name, Commencement_Date__c = dtStartDate, Expiration_Date__c = dtEndDate, 
                                                     Account = objAccount, AccountId = objAccount.Id, Document_Type__c = 'Partnership Marketing', Deal_Type__c = 'Sponsorship', Primary_Contact__c = contactId,
                                                     Signatory__c = contactId, StageName = StageName, CloseDate = date.today().addDays(10), DBA__c = dbaId,
                                                     Payment_Frequency__c = PaymentFrequency, Amount = 7000);
                                                             
        return objOpportunity;
    }
    
    ///Creating OpportunityLineItem
    public static OpportunityLineItem createOLI(Id objPBEId, Id OpportunityId)
    {
        OpportunityLineItem objOLI = new OpportunityLineItem(PricebookEntryId = objPBEId, Quantity = 20,OpportunityId = OpportunityId, TotalPrice = 1000);
        return objOLI;
    }
    
    //Created to set start date and end date for opportunity line item Bn
    public static OpportunityLineItem createOLI(Id objPBEId, Id OpportunityId,Date stDate, Date endDate)
    {
        OpportunityLineItem objOLI = new OpportunityLineItem(PricebookEntryId = objPBEId, Quantity = 20,OpportunityId = OpportunityId, TotalPrice = 1000,Start_Date__c = stDate, End_Date__c = endDate);
        return objOLI;
    }
    
    ///Creating Product2
    public static Product2 createProduct(String productName, Integer intAvailability, String strBillCode, Date startDate, Date endDate, String strInventoryType, Id inventoryId, String strCategory, String strRegion, String strLocation, String strRateType, Boolean blnActive)
    {
        Product2 objProduct = new Product2(Name = productName, Availability__c = intAvailability, Bill_Code__c = strBillCode, Start_Date__c = startDate, End_Date__c = endDate,
                                        Inventory_Type__c = strInventoryType, Property__c = inventoryId, Inventory_Categorization__c = strCategory, Region__c = strRegion,
                                        Location__c = strLocation, Rate_Type__c = strRateType, isActive = blnActive);
        return objProduct;
    }
    
    ///Creating Property__c
    public static Property__c createProperty(String strPropertyName)
    {
        Property__c objProperty = new Property__c(Name = strPropertyName);
        return objProperty;
    }
    
    ///Creating Account
    public static Account createAccount(String strAccountName)
    {
        Account objAccount = new Account(Name = strAccountName);
        return objAccount;
    }
    
    ///Creating DBA__c
    public static DBA__c createDBA(Id idAccount)
    {
        DBA__c objDBA = new DBA__c(Account__c = idAccount);
        return objDBA;
    }
    
    ///Creating Contact
    public static Contact createContact(String strContactName, Account objAccount)
    // 2015-03-25 johnsondw Set MailingState = 'PA' because of the new validation.
    {
        Contact objContact = new Contact(LastName = strContactName, Account = objAccount, AccountId = objAccount.Id, MailingState = 'PA');
        return objContact;
    }
    
    ///Creating PricebookEntry
    public static PricebookEntry createPBE(Id spId, Integer intUnitPrice, Id idProduct, Boolean blnStdPrice, Boolean blnActive)
    {
        PricebookEntry objPBE = new PricebookEntry(Pricebook2Id = spId, UnitPrice=intUnitPrice, Product2Id=idProduct, UseStandardPrice = blnStdPrice, isActive=blnActive);
        return objPBE;
    }
    
    
    /*end : Create Objects required for above created test cases*/

}