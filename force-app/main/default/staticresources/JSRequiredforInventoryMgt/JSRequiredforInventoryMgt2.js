var arrCheckBox = new Array();

///prototype for finding months between 2 dates
Date.prototype.MonthsBetween = function()
{  
	var date1,date2,negPos;  
	if(arguments[0] > this){  
	  date1 = this;  
	  date2 = arguments[0];  
	  negPos = 1;  
	}  
	else{  
	  date2 = this;  
	  date1 = arguments[0];  
	  negPos = -1;  
	}  
	
	if(date1.getFullYear() == date2.getFullYear()){  
	  return negPos * (date2.getMonth() - date1.getMonth());  
	}  
	else{  
	  var mT = 11 - date1.getMonth();  
	  mT += date2.getMonth() + 1;  
	  mT += (date2.getFullYear() - date1.getFullYear() - 1) * 12;  
	  return negPos * mT;        
	}  
}

var inputId = new Array();
var arrHoverOver = new Array();

// Quantity for atleast one product should be greater than zero if clicked on Add Qty Inventory. 
function validateQtyWhileAddingToInventory()
{
	var flagToAvoidAdditionToInventory = true;
	for(var i=0 ; i < inputId.length; i++)
	{
		if(inputId[i].value > 0)
			flagToAvoidAdditionToInventory = false;                                                	                                                		                                                                                                                                               
	}   
	if(flagToAvoidAdditionToInventory)
	{
		alert("Please select a product by adding some quantity");
		return false;
	}   
	else 
	{
		AddInventory();
	}                                 
}

function convertDate(inputFormat) 
{
  var d = new Date(inputFormat);
  return [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/');
}

//Qty to be non negative.
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}


function validateQuantity(quantityId, varAvailability)
 {	
	 //Qty to be within maximum availability set at the product level.
	 var enteredQty = parseInt(document.getElementById(quantityId).value);
	 var totalAvailability = parseInt(varAvailability);

	 if(totalAvailability != -1)
	 {	 
		 if(varAvailability != null && varAvailability != '' && enteredQty > totalAvailability)
		 {
			 alert('Quantity cannot be greater than Availability and the maximum quantity available is '+ varAvailability);
			 document.getElementById(quantityId).value = 0;		 
		 }  
     }
	 calculateGrandTotals();	 
 } 
 
 function clearQty()
 {
	for(var i=0 ; i < inputId.length; i ++)
	{
		inputId[i].value = 0;
	}
	
 }
 
function validateRemoval()
{
	var flagToAvoidRemoval = true;
	if(arrCheckBox.length > 0 )
	{
		for(var i=0 ; i < arrCheckBox.length; i++)
		{
			if(arrCheckBox[i].checked)
			{
				flagToAvoidRemoval = false;
			}                                                	                                                		                                                                                                                                               
		}
	}
		   
	if(flagToAvoidRemoval)
	{
		alert("Please select a product to be removed");
		return false;
	}   
	else 
		RemoveInventory();                                 
}
	
function checkAll(cb)
{
	var inputElem = document.getElementsByTagName('input');
	for(var i=0; i<inputElem.length; i++)
	{
		inputElem[i].checked = cb.checked;
		
		if(cb.checked)
		{
			var id = cb;
			arrCheckBox.push(id);
		}
		else if(!cb.checked)
		{
			var idIndex = new Array();
			for(var j=0 ; j < arrCheckBox.length; j++)
			{
				idIndex.push(arrCheckBox[j].id);
				if(idIndex[j].indexOf(inputElem[i].id) > -1)
				{
					var index = idIndex[j].indexOf(inputElem[i].id);
					if (index > -1) 
					{
						arrCheckBox.splice(index, 1);
					}
				}
			}
		}
	}
}  

///update total amount dynamically
function updateTotals(rateType, quantityId, unitPriceId, totalId, colTotal, row, idGrandTotal, varAvailability, OLIQuantity, startDate, endDate, numberOfDays, originalStartDate, originalEndDate, diffDaysInputHidden, hiddenvalue, AvailabilityOnProduct)
{
	var enteredQty = parseInt(document.getElementById(quantityId).value);
	var totalAvailability = parseInt(AvailabilityOnProduct);
	 
	if(totalAvailability != null && enteredQty > totalAvailability)
	{
		alert('Quantity cannot be greater than Availability and the maximum quantity available is '+ totalAvailability);
		document.getElementById(quantityId).value = 0;		 
	}  

////////////
	var quantity = document.getElementById(quantityId).value;
	/*//document.getElementById(quantityId).value = quantity.replace(/[^a-zA-Z0-9]+/g,'');
	
	if(parseInt(document.getElementById(quantityId).value) > parseInt(QtyRemaining) && (varAvailability != 0 && varAvailability != ''))
	{
		alert('Quantity Cannot be greater than Availability and the maximum quantity available is '+ QtyRemaining);
		document.getElementById(quantityId).value = 0;                                                
	}*/
	
	var objTable = row.parentNode;
	
	var cost = document.getElementById(unitPriceId).innerHTML; 
	cost = cost.replace(',','').trim();
	var RateType =  document.getElementById(rateType).innerHTML;
	var strStartDate = document.getElementById(startDate).value;
	
	var strEndDate = document.getElementById(endDate).value;
	var firstDate = new Date(strStartDate.split('/')[2], strStartDate.split('/')[0] - 1, strStartDate.split('/')[1] );
	var secondDate = new Date(strEndDate.split('/')[2], strEndDate.split('/')[0] - 1, strEndDate.split('/')[1] ) ;
	
	var totalPrice = 0;
	var days = 1000 * 60 * 60 * 24;
	var diffDays = 0;
	//diffDays = Math.round((((secondDate - firstDate))/days)) + 1;
	
	if(firstDate == 'Invalid Date' || secondDate == 'Invalid Date' || secondDate == '')
	{
		alert('Please enter start and end dates');
		
		var varStart = new Date(convertDate(originalStartDate));
		firstDate = varStart;
		varStart = [varStart.getMonth()+1, varStart.getDate(), varStart.getFullYear()].join('/');
		
		var varEnd = new Date(convertDate(originalEndDate));
		secondDate = varEnd;
		varEnd = [varEnd.getMonth()+1, varEnd.getDate(), varEnd.getFullYear()].join('/');
		
		if(firstDate == 'Invalid Date')
			document.getElementById(startDate).value = varStart;
		if(secondDate == 'Invalid Date')
			document.getElementById(endDate).value = varEnd;
		
		diffDays = Math.round((((secondDate - firstDate))/days)) + 1;
	}
	else 
	{
		diffDays = Math.round((((secondDate - firstDate))/days)) + 1;
		 if(RateType == 'Daily')
		 {
			 totalPrice = diffDays * quantity * cost;
		 }
		 else if(RateType == 'Term' || RateType == '' || RateType == '--None--' || RateType == undefined)
		 {
			totalPrice = 1 * quantity * cost;
			
		 }
	
	if(!isNaN(totalPrice))
		document.getElementById(totalId).value = totalPrice.toFixed(2);
		
	if(document.getElementById(quantityId).value == 0)
		document.getElementById(totalId).value = 0;
	}
	
	document.getElementById(numberOfDays).innerHTML = diffDays;
	document.getElementById(numberOfDays).value = diffDays;
	document.getElementById(hiddenvalue).value = diffDays;
	
	calculateGrandTotals();
}

 function storePreviousDateValue(StartOrEndDate)
 {
	 var txtDate = document.getElementById(StartOrEndDate);
	 var userInput = new Date(txtDate.value);
	 var hiddenDate = document.getElementById("hiddenIdStartOrEnd");
	 hiddenDate.value = userInput;
  }

function validateDatesOnOpportunity(StartDate, EndDate, originalStartDate, originalEndDate,isStartOrEnd)
{
	var enteredStartDateIsValid = validatedate(StartDate);
	if(enteredStartDateIsValid == false)
			return false;
    var enteredEndDateIsValid = validatedate(EndDate);
	if(enteredEndDateIsValid == false)
			return false;
   
	var txtStartDate = document.getElementById(StartDate);
	var txtEndDate =   document.getElementById(EndDate);
	var hiddenDate = document.getElementById("hiddenIdStartOrEnd");
	var previousdate = new Date(hiddenDate.value);
	
	var dtStartUserInput = new Date(txtStartDate.value);
	var dtEndUserInput = new Date(txtEndDate.value);
	var dtDBStart = new Date(convertDate(originalStartDate));
	var dtDBEnd = new Date(convertDate(originalEndDate));

	if( enteredStartDateIsValid == true && enteredEndDateIsValid == true)
    {
		if(((dtStartUserInput < dtDBStart || dtStartUserInput > dtDBEnd)|| ( dtEndUserInput> dtDBEnd || dtEndUserInput < dtDBStart)) || (dtStartUserInput > dtEndUserInput ))
	    {
			alert('Please Enter dates between Opportunity start and end dates.');
			previousdate = [previousdate.getMonth()+1, previousdate.getDate(), previousdate.getFullYear()].join('/');
			if(isStartOrEnd == 'StartDate')
				txtStartDate.value = previousdate;
			if(isStartOrEnd == 'EndDate')
				txtEndDate.value = previousdate;
			
	    }
     }
} 

 
function validatedate(enteredDate)  
  {  
        var txtDate = document.getElementById(enteredDate);
	    var dtUserInput = new Date(txtDate.value);
		var hiddenDate = document.getElementById("hiddenIdStartOrEnd");
		var previousdate = new Date(hiddenDate.value);
		//var varStart = new Date(OriginalDate);

	    if(dtUserInput != 'Invalid Date' &&  dtUserInput != '' )  
	    {  
          var splitDate = txtDate.value.split('/');  
          var month  = parseInt(splitDate[0]);  
          var day = parseInt(splitDate[1]);  
          var year = parseInt(splitDate[2]);  

         // Create list of days of a month [assume there is no leap year by default]  
          var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  

		  if( month <= 12)
		 {
            if (month == 1 || month > 2)  
            {  
		      if (day>ListofDays[month-1])  
		      {  
			       alert('Please enter valid date');  
			       previousdate = [previousdate.getMonth()+1, previousdate.getDate(), previousdate.getFullYear()].join('/');
			       txtDate.value = previousdate;
			       return false;
		       }   
             }  
            if (month == 2)  
            {  
	           var leapyear = false;  
               if ( (!(year % 4) && year % 100) || !(year % 400))   
               {  
                  leapyear = true;  
               }  
               if ((leapyear == false) && (day >= 29))  
               {  
                  alert('Please enter valid date');
			      previousdate = [previousdate.getMonth()+1, previousdate.getDate(), previousdate.getFullYear()].join('/');
			      txtDate.value = previousdate;
		          return false; 
               }  
	           if ((leapyear == true) && (day > 29))  
	           {  
				 alert('Please enter valid date'); 
			     previousdate = [previousdate.getMonth()+1, previousdate.getDate(), previousdate.getFullYear()].join('/');
			     txtDate.value = previousdate;
	             return false;
	           }  
            } 
		    return true;
		  }

		 else
		{
			alert('Please enter valid date'); 
			previousdate = [previousdate.getMonth()+1, previousdate.getDate(), previousdate.getFullYear()].join('/');
			txtDate.value = previousdate;
	        return false;
		}
       }  
      else  
      {  
	      alert("Please enter valid date"); 
		  previousdate = [previousdate.getMonth()+1, previousdate.getDate(), previousdate.getFullYear()].join('/');
		  txtDate.value = previousdate;
          return false;
      }  
  }  

function convertDate(inputFormat) 
{
	var d = new Date(inputFormat);
	return [d.getMonth()+1, d.getDate(), d.getFullYear()].join('/');
}