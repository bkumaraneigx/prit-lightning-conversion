/*
This trigger will recalculate allocation maps after a opportunity line item deleted.
*/
trigger Trigger_OppLineItem on OpportunityLineItem (after delete) 
{
	List<String> lstOppIds = new List<ID>();
	Integer countOfOppLIItems=0;
	String oppId;
	for(OpportunityLineItem oppLIObj: trigger.OldMap.values())
	{
		oppId = oppLIObj.OpportunityId;
		lstOppIds.add(oppId);
		
	}
	
	System.debug('Opp lineitem delete trigger calld with'+lstOppIds + 'oppId'+ oppId);
	List<Opportunity> lstOpp = [SELECT ID, CountofOpportunityInventory__c FROM Opportunity where id =: lstOppIds];
	//Get all opportunities and delete the allocation maps for the opportunites which has zero opp line items only.
	if(lstOpp.size() > 0)
	{
		AllocationTable.deleteAllocationTablesForNoOppLI(lstOppIds);		
	}
	
	//Call Allocation map to recalculate the allocation table because the opportunity line item is deleted
	
	
    
}