trigger SL_OpportunityTrigger on Opportunity (before insert, after insert, before update, after update) 
{
    // initialize the handler class Opportunity
    SL_OpportunityHandler handler = new SL_OpportunityHandler(Trigger.isExecuting, Trigger.size); 
      
    User objUser = new User();
	
	if(!Test.isRunningTest())
		objUser = [SELECT Id, Name, Informatica_Cloud_User_Name__c, Informatica_Cloud_Password__c FROM User WHERE Id =: UserInfo.getUserId() limit 1];
    
    if(trigger.isBefore && trigger.isInsert)
    {
    	handler.onBeforeInsert(trigger.new);
    }
     
    if(trigger.isBefore && trigger.isUpdate)
    {
    	handler.onBeforeUpdate(trigger.new);
    }
    
    // fires on After Insert of the Opportunity
    if(trigger.isAfter && trigger.isInsert && !SL_RecursionHelper.hasAlreadyCreatedOpportunity())
    {
    	///PREIT-6, Integration with Informatica
        if(objUser.Informatica_Cloud_User_Name__c != null && objUser.Informatica_Cloud_User_Name__c != '' &&
            objUser.Informatica_Cloud_Password__c != null && objUser.Informatica_Cloud_Password__c != '')
        {
            //ClosedOpportunityIntegration.runJob(objUser.Informatica_Cloud_User_Name__c,objUser.Informatica_Cloud_Password__c,'SF_To_JDE_Opportunities','DSS');
        }
        
        ///To handle recursion
        SL_RecursionHelper.setAlreadyCreatedOpportunity();
        SL_RecursionHelper.setAlreadyUpdatedOpportunity();
        handler.onAfterInsert(trigger.OldMap,trigger.NewMap);
    }
    
    
    if(Test.isRunningTest() && SL_RecursionHelper.hasAlreadyUpdatedOpportunity())
    	SL_RecursionHelper.isRecursive = false;
    // fires on After Update of the Opportunity
    if(trigger.isAfter && trigger.isUpdate && !SL_RecursionHelper.hasAlreadyUpdatedOpportunity())
    {
    	///PREIT-6, Integration with Informatica 
    	if((objUser.Informatica_Cloud_User_Name__c != null && objUser.Informatica_Cloud_User_Name__c != '' &&
            objUser.Informatica_Cloud_Password__c != null && objUser.Informatica_Cloud_Password__c != '' )
            && ((Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()) || (!Test.isRunningTest())))
        {
        	//ClosedOpportunityIntegration.runJob(objUser.Informatica_Cloud_User_Name__c,objUser.Informatica_Cloud_Password__c,'SF_To_JDE_Opportunities','DSS');
        }
        
        ///To handle recursion
    	SL_RecursionHelper.setAlreadyUpdatedOpportunity();
    	SL_RecursionHelper.setAlreadyCreatedOpportunity();
    	handler.onAfterUpdate(trigger.OldMap,trigger.NewMap);
    } 
    
    if(Test.isRunningTest())
    	SL_RecursionHelper.isRecursive = false;
}