({
 doInit: function(component, event, helper) {
   var action = component.get("c.getRegions");
    action.setCallback(this, function(data) {
        /* var myMap = [];
        var curObj = data.getReturnValue();
      for(var key in curObj) {
          myMap.push({rname: curObj[key]});
      }*/
      // component.set("v.Inventory", myMap);
      component.set("v.Inventory", data.getReturnValue());
      console.log('Regions ----->', data.getReturnValue());
    });
    $A.enqueueAction(action);
   var action2 = component.get("c.getRegionLocations");
    action2.setCallback(this, function(a) {
        console.log('Locations for regions ----->', a.getReturnValue());
   component.set("v.InventoryLocation", a.getReturnValue());
});
$A.enqueueAction(action2);    
  },

checkboxSelect: function(component, event, helper) {
         
    var getAllCheckboxes = component.find("chkOuter");
    var getInnerCheckboxes = component.find("chkInner");
    var getAllCheckDiv = component.find("chkdiv");
    var sChecked = "";
    var innerCheckBoxesChecked = "";
    for (var i = 0; i < getAllCheckboxes.length; i++) {
        if (getAllCheckboxes[i].getElement().checked) {
            if (sChecked === "") {
                sChecked = getAllCheckboxes[i].getElement().value;
            } else {
                sChecked = sChecked + "," + getAllCheckboxes[i].getElement().value;
            }
            
            //alert('Schecked value is : ----->>>>> ' + sChecked);
            if (getAllCheckDiv[i].getElement().style.display === "none") {
            getAllCheckDiv[i].getElement().style.display = "block";
            }
        }
        if (!getAllCheckboxes[i].getElement().checked) {
            getAllCheckDiv[i].getElement().style.display = "none";
        }
    }
    for (var i = 0; i < getInnerCheckboxes.length; i++) {
        if (getInnerCheckboxes[i].getElement().checked) {
            if (innerCheckBoxesChecked === "") {
                innerCheckBoxesChecked = getInnerCheckboxes[i].getElement().value;
            } else {
                innerCheckBoxesChecked = innerCheckBoxesChecked + "," + getInnerCheckboxes[i].getElement().value;
            }
            
            console.log('Schecked value is : ----->>>>> ' + sChecked);
        }
    }
     var SelectedStates = $A.get("e.c:property_event");
		SelectedStates.setParams({ "selectedPropertyIds" : sChecked });
		SelectedStates.fire();
    var selectedCities = $A.get("e.c:cities_event");
    selectedCities.setParams({ "selectedCitiesIds" : innerCheckBoxesChecked });
		selectedCities.fire();

},
checkboxSelectAll: function(component, event, helper) {
       var chkboxes = component.find('chkOuter');
       var chkSelAll = component.find('chk1SelAll');
       var getAllCheckDiv = component.find("chkdiv");
        
       var y = 0;
       for (y = 0; y < chkboxes.length; y++) {
           chkboxes[y].getElement().checked = chkSelAll.getElement().checked;
           if (chkboxes[y].getElement().checked) {
               if (getAllCheckDiv[y].getElement().style.display === "none") {                   
                getAllCheckDiv[y].getElement().style.display = "block";
               }
           } else {
               getAllCheckDiv[y].getElement().style.display = "none";
           }
        }
        var chkInner2 = component.find('chkInner');
        for (y = 0; y < chkInner2.length; y++) {
           chkInner2[y].getElement().checked = chkSelAll.getElement().checked;
        }
},
 expandAll: function(component, event, helper) {
       var chkboxes = component.find('chkOuter');
       var chkSelAll = component.find('chk1SelAll');
       var getAllCheckDiv = component.find("chkdiv");
    
       var y = 0;
       for (y = 0; y < chkboxes.length; y++) {
           chkboxes[y].getElement().checked = chkSelAll.getElement().checked;         
           if (getAllCheckDiv[y].getElement().style.display === "none") {
               getAllCheckDiv[y].getElement().style.display = "block";
           }
        }
},
 collapseAll: function(component, event, helper) {
       var chkboxes = component.find('chkOuter');
       var chkSelAll = component.find('chk1SelAll');
       var getAllCheckDiv = component.find("chkdiv");
    
       var y = 0;
       for (y = 0; y < chkboxes.length; y++) {
           getAllCheckDiv[y].getElement().style.display = "none";
        }
}
    

 })