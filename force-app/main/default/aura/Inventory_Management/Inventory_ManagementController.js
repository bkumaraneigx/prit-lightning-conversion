({
    doInit : function(component, event, helper) {
        
        $( document ).ready(function() {
    		alert( "ready!" );
		});
        
        console.log("Record Id is : " , component.get('v.recordId'));
        var pageRef = component.get("v.pageReference");
        console.log('Page Refernce is : ' + pageRef);
        console.log('Page ref JSON is : ' + JSON.stringify(pageRef));
        var id = component.get("v.oppId");
        console.log("recordId from calling comp is --> " + id);
        
        var today = new Date();
    		var monthDigit = today.getMonth() + 1;
    		if (monthDigit <= 9) {
        	monthDigit = '0' + monthDigit;
    	}
        component.set("v.currentDate", today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
        console.log('Current Date =====' + component.get("v.currentDate"));
        
        //var curDate = component.find("startDate");
        //curDate.getElement().innerHTML = new Date();
        //console.log('CurrentDate ===== ', curDate);
        //component.get("v.currentDate", curDate);
        
        var columns = [
            {
                type: 'text',
                fieldName: 'location',
                label: 'Location',
                initialWidth: 300
            }
        ];

        component.set('v.gridColumns', columns);

        // data
        var nestedData = [
            {
                "location": "New Jersey",
                "_children": [
                    {
                        "location": "Princeton",
                        "location": "Mt.Laurel"
                    }
                ]
            },

            {
                
                "location": "Pennsylvania",
                "_children": [
                    {
                        "location": "Philadelphia",
                        "location": "Harrisburg"
                    }
                ]
            }
        ];

        component.set('v.gridData', nestedData);

        component.set('v.columns', [
            {label: 'Opportunity name', fieldName: 'opportunityName', type: 'text'},
            {label: 'Account name', fieldName: 'accountName', type: 'text'}
        ]);

        component.set('v.data', [
            {"opportunityName": "Opp-1", "accountName": "Account-1"},
            {"opportunityName": "Opp-2", "accountName": "Account-2"},
            {"opportunityName": "Opp-3", "accountName": "Account-3"},
            {"opportunityName": "Opp-4", "accountName": "Account-4"},
            {"opportunityName": "Opp-5", "accountName": "Account-5"}
        ]);
        
       	/*var product2s = component.get("c.getAllProducts");
        product2s.setCallback(this, function(data) {
            // var state = data.getResponse(); 
            var returnedproducts = data.getReturnValue();
            component.set("v.products", returnedproducts);
            console.log("Products are ----- > "+ JSON.stringify(returnedproducts));
            component.set("v.showProductsTable", "true");
        });
        $A.enqueueAction(product2s);*/
        
        /*var testAction = component.get("c.fetchProducts"); 
		testAction.setCallback(this, function(data) {
            // var state = data.getResponse(); 
            var returnedproducts = data.getReturnValue();
            //component.set("v.products", returnedproducts);
            console.log("Products are ----- > "+ JSON.stringify(returnedproducts));
            //component.set("v.showProductsTable", "true");
        });
        $A.enqueueAction(testAction);*/

        //var expandedRows = ["123556", "123556-A", "123556-B", "123556-B-B", "123557"];

        //cmp.set('v.gridExpandedRows', expandedRows);
    },
    search : function(component, event, helper) {
        var startDate = component.find("stDate");
        var endDate = component.find("enDate");
        console.log('St date : ' + startDate);
        console.log('EN date : ' + endDate);
        var startDateValue = startDate.getElement().value;
        var endDatevalue = endDate.getElement().value;
        console.log("Start Date is : " + startDateValue);
        console.log("End Date is : " + endDatevalue);
        var inventoryCategory = component.get("v.categories");
        var inventorySubCategory = component.get("v.subCategories");
        var states = component.get("v.states");
        var cities = component.get("v.cities");
        console.log("inventoryCategory is : " + component.get("v.categories"));
        console.log("inventorySubCategory is : " + component.get("v.subCategories"));
        console.log("states : " + component.get("v.states"));
        console.log("cities : " + component.get("v.cities"));
        
        
        var searchAction = component.get("c.getFilteredProducts");
        searchAction.setParams({
            propertyStates : states,
            propertyCities : cities,
            categories : inventoryCategory,
            subCategories : inventorySubCategory,
            startDate : startDateValue,
            endDate : endDatevalue
        });
        searchAction.setCallback(this, function(data) {
            var state = data.getState();
            if (state == "SUCCESS") {
                var filteredProducts = data.getReturnValue();
                component.set("v.products", filteredProducts);
                console.log('Filtered Producte are ; ' + JSON.stringify(filteredProducts));
            } else {
                console.error('Some error -->');
            }
        });
        $A.enqueueAction(searchAction);
    },
    setStartDateAsToday: function(component, event, helper) {
        var start = component.find("stDate");
        start.getElement().value = component.get("v.currentDate");
    },
    setEndDateAsToday: function(component, event, helper) {
        var end = component.find("enDate");
        end.getElement().value = component.get("v.currentDate");
    },
    handleApplicationEvent : function(component, event, helper) {
        component.set('v.states', "");
       var name = event.getParam("selectedPropertyIds");
        alert('Name is : ' + name);
        component.set('v.states', name);
        console.log('States now are : *******  ' + component.get('v.states'));
    	//var getTestTxt = component.find("test");
         //getTestTxt.getElement().value = name;
       //alert(getTestTxt.getElement().value);
   },
    handleCityEvent : function(component, event, helper) {
        component.set('v.cities', "");
       var name = event.getParam("selectedCitiesIds");
        alert('Name is : ' + name);
        component.set('v.cities', name);
        console.log('cities now are : *******  ' + component.get('v.cities'));
    	//var getTestTxt = component.find("test");
         //getTestTxt.getElement().value = name;
       //alert(getTestTxt.getElement().value);
   },
    handleCategorySelection : function(component, event, helper) {
        component.set('v.categories', "");
       var name = event.getParam("selectedCategoryIds");
        alert('Name is : ' + name);
        component.set('v.categories', name);
        console.log('Categories now are : *******  ' + component.get('v.categories'));
    	//var getTestTxt = component.find("test");
         //getTestTxt.getElement().value = name;
       //alert(getTestTxt.getElement().value);
   },
    handleSubCategorySelection : function(component, event, helper) {
        component.set('v.subCategories', "");
       var name = event.getParam("selectedSubCategoryIds");
        //splitString = name.split(",");
        alert('Name is : ' + name);
        component.set('v.subCategories', name);
        console.log('subCategories now are : *******  ' + component.get('v.subCategories'));
    	//var getTestTxt = component.find("test");
         //getTestTxt.getElement().value = name;
       //alert(getTestTxt.getElement().value);
   },
    validateQuantity: function(component, event, helper) {
        var availableQty = event.getSource().get('v.name');
        var currentQty = event.getSource().get('v.value');
        var eleId = event.getSource().get('v.id');
        var ele = component.find(eleId);
        var idx = component.get("v.indexVar");
        
        var prods = component.get("v.products");
        console.log('CurrentIte is ************ : ' + JSON.stringify(prods[idx]));
        
        console.log('Eleis ; ' + ele);
        var currentTarget = event.currentTarget;
        
        console.log('Current Target is : ' + currentTarget);
        
     
        console.log("********** Event is : " + JSON.stringify(event));
        
        if (availableQty < currentQty) {
            alert('Please check the value for entered quantity');
            prods[idx].qty__c = 0;
            component.set("v.products", prods);
            console.log('CurrentIte is : ' + JSON.stringify(component.get("v.products")[idx]));
            
        } else {
            prods[idx].qty__c = currentQty;
            component.set("v.products", prods);
            console.log('CurrentIte is : ' + JSON.stringify(component.get("v.products")[idx]));
        }
        //var selectedItem = event.currentTarget; // Get the target object
     	//var index = selectedItem.dataset.record; // Get its value i.e. the index
     	//var currentObj = cmp.get("v.products")[index];
        //sconsole.log('current obj ; ' + currentObj);
        console.log('Avail Qty is : ' + JSON.stringify(availableQty));
    },
    currentItemVal : function(cmp, event, helper) {
        console.log('Event is : ' + event);
    	//var currentObj = event.getSource().get('v.value');
        //cmp.set("v.currentObject", currentObj);
        var selectedItem = event.currentTarget; // Get the target object
     	var index = selectedItem.dataset.record; // Get its value i.e. the index
        console.log('Selected Item is : ' + selectedItem);
        console.log('Index is : ' + index);
        cmp.set("v.indexVar", index);
     	var currentObj = cmp.get("v.products")[index]; // Use it retrieve the store record 
        cmp.set("v.currentObject", currentObj);
        console.log('current item is : ' + JSON.stringify(currentObj));
	},
    validateInputQuantity: function(component, event, helper) {
        var availableQty = event.getSource().get('v.name');
        var currentQty = event.getSource().get('v.value');
        var eleId = event.getSource().get('v.id');
        var curObj = component.get("v.currentObject");
        
        console.log('Id is : ' + eleId);
     
        console.log("********** Event is : " + JSON.stringify(event));
             
        var ele = $(("#"+eleId)).html();
        console.log("Ele is -----> " + ele);
        
        
        console.log('Element Found is : ' + ele);

        if (availableQty < currentQty) {
            alert('Please check the value for entered quantity');
            curObj.qty__c = 0;
            console.log('Current Obj is : ' + JSON.stringify(curObj));
        } else {
            curObj.qty__c = currentQty;
        }
        console.log('Current qty is : ' + currentQty);
        //var selectedItem = event.currentTarget; // Get the target object
     	//var index = selectedItem.dataset.record; // Get its value i.e. the index
     	//var currentObj = cmp.get("v.products")[index];
        //sconsole.log('current obj ; ' + currentObj);
        console.log('Avail Qty is : ' + JSON.stringify(availableQty));
    }
})