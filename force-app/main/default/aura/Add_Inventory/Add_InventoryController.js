({
	navigateToInventoryManagement : function(component, event, helper) {
        console.log("Record Id is : ", component.get("v.recordId"));
		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:Inventory_Management",
            componentAttributes : {
               oppId : component.get("v.recordId")
            } 
        });
        evt.fire();
    } 
})