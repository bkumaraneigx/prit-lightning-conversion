({
	doInit : function(component, event, helper) {
		var product2s = component.get("c.getAllProducts");
        product2s.setCallback(this, function(data) {
            // var state = data.getResponse(); 
            var returnedproducts = data.getReturnValue();
            var limitedProducts = [];
            limitedProducts.push(returnedproducts[0]);
            limitedProducts.push(returnedproducts[1]);
            component.set("v.products", limitedProducts);
            console.log("Products are ----- > "+ JSON.stringify(returnedproducts));
        });
        $A.enqueueAction(product2s);
	}
})