({
	doInit : function(component, event, helper) {
		var product2s = component.get("c.getAllProducts");
        product2s.setCallback(this, function(data) {
            // var state = data.getResponse(); 
            var returnedproducts = data.getReturnValue();
            component.set("v.products", returnedproducts);
            console.log("Products are ----- > "+ JSON.stringify(returnedproducts));
        });
        $A.enqueueAction(product2s);
	}
})